import React, {Component} from 'react';
import { TouchableOpacity, Platform } from 'react-native';
import { Icon } from 'native-base'

export default class CustomerService extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity onPress={()=>{this.props.navigation.navigate("ScanScreen")}} style={{backgroundColor:'#65c5f2' ,width:50, height:50, borderRadius:25, borderColor:'#65c5f2', borderWidth:2, position:'absolute', bottom:Platform.OS === 'ios' ? 100 :70, right:20, alignItems:'center', justifyContent:'center'}}>
        <Icon type="FontAwesome5" name="qrcode" style={{fontSize:30, color:"white"}}/>
      </TouchableOpacity>
    );
  }
}
