import React, {Component} from 'react';
import {View, Image, TouchableOpacity, FlatList, Dimensions, Text} from 'react-native';
import { Footer, FooterTab, Button, Icon, Badge } from 'native-base'
import PropTypes from 'prop-types';
import Modal from "react-native-modal";

export default class EmptyBasket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  static propTypes = {
      visible: PropTypes.bool.isRequired,
      onClose: PropTypes.func.isRequired
  };

  render() {
    return (
      <View>
        <Modal
          isVisible={this.props.visible}
          onBackButtonPress={this.props.onClose}
          onBackdropPress={this.props.onClose}
          backdropOpacity={0.5}
        >
          <View style={{backgroundColor:'white', padding:20}}>
            <TouchableOpacity onPress={this.props.onClose} style={{position:'absolute', top:10, right:10, backgroundColor:'rgba(0,0,0,0.2)', width:40, height:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <Icon type="FontAwesome" name='close' style={{fontSize:20, color:'white'}} />
            </TouchableOpacity>
            <Image style={{height:150, width:150, resizeMode:'contain', alignSelf:'center', marginTop:20}} source={require('../assets/img/logo.png')} />
            <Text style={{alignSelf:'center', fontSize:20, color:'black', fontWeight:'bold', marginTop:20}}>Belum ada barang tambahan</Text>
            <Text style={{alignSelf:'center', textAlign:'center', marginTop:10, fontSize:14}}>Wujudkan impian di wishlist kamu</Text>
            <Button onPress={this.props.onClose} style={{alignSelf:'center', width:'90%', justifyContent:'center', marginTop:50, marginBottom:20, borderRadius:8, backgroundColor:"#65c5f2"}}>
              <Text style={{color:'white', fontWeight:'bold'}}>Load More</Text>
            </Button>
          </View>
        </Modal>
      </View>
    );
  }
}
