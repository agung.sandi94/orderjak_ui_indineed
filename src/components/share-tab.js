import React, {Component} from 'react';
import {View, Image, TouchableOpacity, FlatList, Dimensions, Text} from 'react-native';
import { Footer, FooterTab, Button, Icon, Badge, Card } from 'native-base'
import Modal from "react-native-modal";

export default class ShareTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false
    };
  }

  render() {
    return (
      <Modal
        isVisible={this.props.isVisible}
        onBackButtonPress={this.props.onClose}
        onBackdropPress={this.props.onClose}
        onSwipeComplete={this.props.onClose}
        swipeDirection={['down']}
        style={{justifyContent: 'flex-end', margin: 0}}
      >
        <View style={{backgroundColor:'white', width:'100%', height:'47%', borderTopLeftRadius:20, borderTopRightRadius:20, paddingBottom:50}}>
          <View style={{width:'100%', height:'35%'}}>
            <View style={{height:20, width:Dimensions.get('window').width - 40, justifyContent:'center', alignItems:'center', marginLeft:20, marginRight:20}}>
              <TouchableOpacity style={{backgroundColor:'lightgray', width:'12%', height:'25%'}} onPress={this.props.onClose}></TouchableOpacity>
            </View>
            <Text style={{marginLeft:20, marginTop:10}}>Order Jak</Text>
            <Text style={{marginLeft:20, marginTop:5, fontSize:22, fontWeight:'bold', color:'black'}}>Share this post on :</Text>
          </View>
          <View style={{width:'100%', height:'65%'}}>
            <View style={{width:'100%', height:'50%', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
              <View style={{alignItems:'center', justifyContent:'center', margin:5}}>
                <Card style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                    <Icon type="FontAwesome" name="google-plus" style={{fontSize:24, color:'red'}} />
                  </TouchableOpacity>
                </Card>
                <Text style={{fontSize:12, fontWeight:'500', marginTop:5}}>Google+</Text>
              </View>
              <View style={{alignItems:'center', justifyContent:'center', margin:5}}>
                <Card style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                    <Icon type="FontAwesome" name="facebook" style={{fontSize:24, color:'blue'}} />
                  </TouchableOpacity>
                </Card>
                <Text style={{fontSize:12, fontWeight:'500', marginTop:5}}>Facebook</Text>
              </View>
              <View style={{alignItems:'center', justifyContent:'center', margin:5}}>
                <Card style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                    <Icon type="FontAwesome" name="twitter" style={{fontSize:24, color:'blue'}} />
                  </TouchableOpacity>
                </Card>
                <Text style={{fontSize:12, fontWeight:'500', marginTop:5}}>Twitter</Text>
              </View>
              <View style={{alignItems:'center', justifyContent:'center', margin:5}}>
                <Card style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                    <Icon type="FontAwesome5" name="line" style={{fontSize:24, color:'green'}} />
                  </TouchableOpacity>
                </Card>
                <Text style={{fontSize:12, fontWeight:'500', marginTop:5}}>Line</Text>
              </View>
            </View>
            <View style={{width:'100%', height:'50%', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
              <View style={{alignItems:'center', justifyContent:'center', margin:5}}>
                <Card style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                    <Icon type="FontAwesome" name="whatsapp" style={{fontSize:24, color:'green'}} />
                  </TouchableOpacity>
                </Card>
                <Text style={{fontSize:12, fontWeight:'500', marginTop:5}}>Whatsapp</Text>
              </View>
              <View style={{alignItems:'center', justifyContent:'center', margin:5}}>
                <Card style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity style={{height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                    <Icon type="FontAwesome5" name="link" style={{fontSize:24, color:'skyblue'}} />
                  </TouchableOpacity>
                </Card>
                <Text style={{fontSize:12, fontWeight:'500', marginTop:5}}>Copy Link</Text>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
