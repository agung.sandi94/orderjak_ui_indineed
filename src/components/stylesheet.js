import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  logo: {
    height:'25%',
    width:'50%',
    resizeMode:'contain'
  },
  title: {
    color:'#65c5f2',
    fontSize:25,
    fontWeight:'bold'
  },
  header: {
    marginTop:25,
    fontWeight:'bold'
  },
  textInput: {
    marginTop:25,
    height:50,
    width:'80%',
    borderRadius:5 ,
    borderColor:'lightgray',
    backgroundColor:'white',
    borderWidth:1,
    paddingLeft:20,
    paddingRight:20
  },
  btnBlue: {
    marginTop:50 ,
    width:'35%',
    backgroundColor:'#65c5f2',
    height:50,
    borderRadius:25,
    alignItems:'center',
    justifyContent:'center'
  },
  textBtnBlue: {
    color:'white',
    fontWeight:'bold',
    fontSize:16
  },
  hyperlink: {
    borderColor:'#65c5f2',
    borderBottomWidth:1
  },
  hyperlinkText:{
    color:'#65c5f2',
    marginLeft:5
  }
});
