import React, {Component} from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Entypo from 'react-native-vector-icons/dist/Entypo';

export default class Wishlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      like: this.props.like,
      isVisible: false
    };
  }

  render() {
    return (
      <TouchableOpacity style={{height:175, flexDirection:'row', paddingBottom:10, paddingTop:10, borderColor:'lightgray'}} onPress={()=>this.props.navigate("IndinewsDetail",{bookmark:this.state.bookmark})}>
        <View style={{width:'30%', height:'90%', paddingTop:10, paddingBottom:10}}>
          <Image style={{height:'100%', width:'100%', borderRadius:5}} source={this.props.img}/>
              <TouchableOpacity onPress={()=>this.setState({like:!this.state.like})}>
                <MaterialIcons name={(this.state.like)?"favorite":"favorite-border"} size={25} color={(this.state.like)?"red":"gray"}/>
              </TouchableOpacity>
        </View>
      </TouchableOpacity>


       
    );
  }
}
