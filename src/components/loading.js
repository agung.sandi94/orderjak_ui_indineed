import React, {Component} from 'react';
import { View, Modal  } from 'react-native';
import PropTypes from 'prop-types';
import { Spinner } from "native-base";

class Loading extends Component {
  static defaultProps = {
    visible: false
  }

  static propTypes = {
    visible: PropTypes.bool
  };

  render() {
      return  (
        <Modal
          animationType="none"
          transparent={true}
          visible={this.props.visible}>
          <View style={{flex: 1, alignItems:'center', justifyContent:'center', backgroundColor:'rgba(0,0,0,0.7)'}}>
            <Spinner color='white'/>
          </View>
        </Modal>
      );
  }

}

export default Loading;
