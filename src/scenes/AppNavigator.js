import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { addNavigationHelpers, createStackNavigator, createAppContainer } from 'react-navigation';
import Login from './login';
import Register from './register';
import Verification from './verification';
import Home from './home';
import Indinews from './indinews';
import IndinewsDetail from './indinews-detail';
import IndikosSearch from './indikos-search';
import KosanDetail from './kosan-detail';
import Maps from './maps';
import IndineedsSearch from './indineeds-search';
import NeedsDetail from './needs-detail';
import TransactionHistoryList from './transaction-history-list';
import KosanList from './kosan-list';
import NeedList from './need-list';
import Wishlist from './wishlist';

import Example from './example';

import ForgotPassword from './forgot-password';
import UbahAlamat from './ubah-alamat';
import IndineedsBasket from './indineeds-basket';
import ProsesPembayaran from './proses-pembayaran';
import PilihAlamatLain from './pilih-alamat-lain';
import FormBooking from './form-booking';
import FormSurvey from './form-survey';
import Comments from './comments';
import TransactionHistory from './transaction-history';
import TransactionHistoryDetail from './transaction-history-detail';
import Chat from './chat';
import Profile from './profile';
import Checkout from './checkout';

const AppNavigator = createStackNavigator({
  // Example: { screen: Example },
  Login: { screen: Login },
  Register: { screen: Register},
  Verification: { screen: Verification},
  Home: { screen: Home },
  Indinews: { screen: Indinews },
  IndinewsDetail: { screen: IndinewsDetail },
  IndikosSearch: { screen: IndikosSearch },
  KosanDetail: { screen: KosanDetail },
  Maps: { screen: Maps },
  IndineedsSearch: { screen: IndineedsSearch },
  NeedsDetail: { screen: NeedsDetail },
  TransactionHistoryList: { screen: TransactionHistoryList },
  KosanList: { screen: KosanList },
  NeedList: {screen: NeedList},
  Wishlist: {screen: Wishlist},

  ForgotPassword: { screen: ForgotPassword },
  UbahAlamat: { screen: UbahAlamat },
  IndineedsBasket: { screen: IndineedsBasket },
  ProsesPembayaran: { screen: ProsesPembayaran },
  PilihAlamatLain: { screen: PilihAlamatLain },
  FormBooking: { screen: FormBooking },
  FormSurvey: { screen: FormSurvey },
  Comments: { screen: Comments },
  TransactionHistoryDetail: { screen: TransactionHistoryDetail },
  TransactionHistory: { screen: TransactionHistory },
  Chat: { screen: Chat },
  Profile: { screen: Profile },
  Checkout: { screen: Checkout }

});

const AppContainer = createAppContainer(AppNavigator);

const mapStateToProps = state => ({
  // nav: state.navReducer,
});

export default connect(mapStateToProps)(AppContainer);
