import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, CardItem, Text, Body, Right, Picker } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';

class PilihAlamatLain  extends Component {
  static navigationOptions = {
    title: "Pilih Alamat Lain",
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff', padding:20}}>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', marginTop:10,}}>
            <View style={{ width:'85%', height:40, paddingLeft:15, paddingRight:15, borderRadius:5, borderWidth:0.5, borderColor:'gray', flexDirection:'row', alignItems:'center'}}>
              <Icon name='search' style={{fontSize:22, color:'gray'}} />
              <Input placeholder='Search' style={{fontSize:14, marginLeft:5}}/>
            </View>
            <TouchableOpacity style={{width:40, height:40, backgroundColor:'#65c5f2', borderRadius:5, alignItems:'center', justifyContent:'center'}}>
              <Icon type="Entypo" name='plus' style={{fontSize:20, color:'white'}} />
            </TouchableOpacity>
          </View>
          <Card style={{marginTop:20}}>
            <CardItem>
              <Text style={{fontSize:12, fontWeight:'bold', color:'black'}}>Mokhamad Fazal</Text>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12, lineHeight:16, color:'gray'}}>
                Istana Paster Regency Jl. Terusan Gn. Batu No.57, Sukaraja, Cicendo, Kota Bandung, Jawa Barat 40175
                Istana Paster Regency Jl. Terusan Gn. Batu No.57, Sukaraja, Cicendo, Kota Bandung, Jawa Barat 40175
                085221711234
              </Text>
            </CardItem>
            <View style={{borderColor:'lightgray', borderTopWidth:0.5, margin:15, marginBottom:0, backgroundColor:'red'}} />
            <CardItem style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <TouchableOpacity style={{flexDirection:'row', alignItems:'center'}} onPress={()=>{this.props.navigation.navigate("UbahAlamat")}}>
                <Icon type="Foundation" name='page-edit' style={{fontSize:22, color:'gray', width:22}} />
                <Text style={{ fontSize:12, color:'gray'}}>Ubah</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon type="FontAwesome" name='check-square' style={{fontSize:22, color:'#65c5f2', width:20}} />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card style={{marginTop:20}}>
            <CardItem>
              <Text style={{fontSize:12, fontWeight:'bold', color:'black'}}>Mokhamad Fazal</Text>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12, lineHeight:16, color:'gray'}}>
                Istana Paster Regency Jl. Terusan Gn. Batu No.57, Sukaraja, Cicendo, Kota Bandung, Jawa Barat 40175
                Istana Paster Regency Jl. Terusan Gn. Batu No.57, Sukaraja, Cicendo, Kota Bandung, Jawa Barat 40175
                085221711234
              </Text>
            </CardItem>
            <View style={{borderColor:'lightgray', borderTopWidth:0.5, margin:15, marginBottom:0, backgroundColor:'red'}} />
            <CardItem style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
              <TouchableOpacity style={{flexDirection:'row', alignItems:'center'}}>
                <Icon type="Foundation" name='page-edit' style={{fontSize:22, color:'gray', width:22}} />
                <Text style={{ fontSize:12, color:'gray'}}>Ubah</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon type="FontAwesome" name='square-o' style={{fontSize:22, color:'#65c5f2', width:20}} />
              </TouchableOpacity>
            </CardItem>
          </Card>
        </Content>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(PilihAlamatLain);
