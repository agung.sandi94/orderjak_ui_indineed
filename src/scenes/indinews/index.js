/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs';
import Listnews from '../../components/listnews';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';

class Indinews extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state={
      activeTab:1,
      bookmark: false
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{flex:1, width:'100%', marginBottom:60, backgroundColor:'white'}}>
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, paddingTop: 20}}>
            <View>
              <Text style={{fontSize:22, fontWeight:'bold', color:'black'}}>Indinews</Text>
              <Text style={{fontSize:12}}>Tuesday, 27 November 2018</Text>
            </View>
            <View>
              <FontAwesome name={"user-circle-o"} size={35} color={"#65c5f2"} />
            </View>
          </View>
          <View>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, paddingTop: 20}}>
              <Text style={{fontSize:18, fontWeight:'bold', width:'90%', color:'black'}}>China Says 2018 Growth Target to Reflect New ...</Text>
              <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'10%'}} onPress={()=>this.setState({bookmark:!this.state.bookmark})}>
                <Image style={{width:25, height:25, resizeMode:'contain'}} source={(this.state.bookmark)?require('../../assets/img/home/bookmark-active.png'):require('../../assets/img/home/bookmark.png')}/>
              </TouchableOpacity>
            </View>
            <ScrollView horizontal={true} pagingEnabled={true} style={{height:220, width:'100%', flexDirection:'row'}}>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:10}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:10}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:10}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:10}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
            </ScrollView>
            <Text style={{marginLeft:10, marginRight:10, fontSize:12}}>9. Blossoming Saints star Alvin Karma become just the third rookie with 600 yards both rushing and receiving in the ...</Text>
          </View>
          <View style={{padding:10}}>
            <ScrollView horizontal={true} style={{borderTopColor:'lightgray', borderBottomColor:'lightgray', borderTopWidth:1, borderBottomWidth:1}}>
              <TouchableOpacity onPress={()=> this.setState({activeTab:1})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==1)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==1)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==1)?'#65c5f2':'black', fontWeight:(this.state.activeTab==1)?'bold':'400'}}>For you</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:2})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==2)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==2)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==2)?'#65c5f2':'black', fontWeight:(this.state.activeTab==2)?'bold':'400'}}>National</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:3})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==3)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==3)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==3)?'#65c5f2':'black', fontWeight:(this.state.activeTab==3)?'bold':'400'}}>Technology</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:4})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==4)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==4)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==4)?'#65c5f2':'black', fontWeight:(this.state.activeTab==4)?'bold':'400'}}>Entertainment</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:5})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==5)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==5)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==5)?'#65c5f2':'black', fontWeight:(this.state.activeTab==5)?'bold':'400'}}>Nature</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:6})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==6)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==6)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==6)?'#65c5f2':'black', fontWeight:(this.state.activeTab==6)?'bold':'400'}}>Location</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View>
            <Listnews
              title="Liburan versi low budget"
              headline="Liburan di tahun baru memang waktu yang pas. Selain banyak promosi ..."
              time="6"
              bookmark={true}
              like={false}
              img={require('../../assets/img/indinews/banner-1.png')}
              navigate={this.props.navigation.navigate}
            />
            <Listnews
              title="Merantau? Gausah galau"
              headline="Apa yang pertama kali kalian pikirkan tentang merantau ? Kebanyakan ..."
              time="40"
              bookmark={false}
              like={true}
              img={require('../../assets/img/indinews/banner-2.png')}
              navigate={this.props.navigation.navigate}
            />
            <Listnews
              title="Berkenalan dengan AIDS"
              headline="Kata AIDS selalu diidentikan dengan HIV. Padahal, kedua hal tersebut ..."
              time="50"
              bookmark={false}
              like={false}
              img={require('../../assets/img/indinews/banner-3.png')}
              navigate={this.props.navigation.navigate}
            />
          </View>
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username,password) => dispatch(login(username,password)),
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Indinews);
