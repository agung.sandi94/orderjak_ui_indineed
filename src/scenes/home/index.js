import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import { login,finishCheck } from './actions'
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import Maintabs from '../../components/maintabs';
import Listnews from '../../components/listnews';
import LoadingView from '../../components/loadingView';
import SplashScreen from 'react-native-smart-splash-screen'

class Home extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state={
      isVisible:false,
      isLoading: false,
    }
  }

  componentDidMount () {
    // SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
    setTimeout(() => {
      this.setState({isLoading:false},()=>{
        SplashScreen.close({
          animationType: SplashScreen.animationType.scale,
          duration: 850,
          delay: 500,
        })
      });
    }, 5000);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{flex:1, width:'100%', marginBottom:60}}>
          <View style={{backgroundColor:'white'}}>
          <LoadingView visible={this.state.isLoading} />
            <View style={{height:300, width:'100%'}}>
              <Image style={{height:'80%', width:'100%', resizeMode:'contain'}} source={require("../../assets/img/home/bg-slider.png")}/>
              <View style={{position:'absolute', top: 0, flexDirection:'row', height:'70%', width:'100%', backgroundColor:'transparent'}}>
                <View style={{flexDirection:'row', height:'35%', width:'75%', alignItems:'center', paddingLeft:20}}>
                  <FontAwesome name={"user-circle-o"} size={35} color={"white"} />
                  <Text style={{color:'white', fontWeight:'bold', marginLeft:10}}>Lauren Mayberry</Text>
                </View>
                <View style={{flexDirection:'row', height:'35%', width:'25%', alignItems:'center', justifyContent:'center', paddingRight:20}}>
                  <FontAwesome name={"minus"} size={16} color={"white"}/>
                  <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
                  <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
                  <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
                </View>
              </View>
              <ScrollView horizontal={true} pagingEnabled={true} style={{position:'absolute', top:'20%', height:'80%', width:'100%', flexDirection:'row'}}>
                <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/home/slider.png")}/>
                </TouchableOpacity>
                <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/home/slider.png")}/>
                </TouchableOpacity>
                <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/home/slider.png")}/>
                </TouchableOpacity>
                <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/home/slider.png")}/>
                </TouchableOpacity>
              </ScrollView>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize:22, fontWeight:'bold', color:'black', marginLeft:15, marginRight:15}}>Explore more</Text>
              <Text style={{marginTop:5, marginLeft:15, marginRight:15}}>Checkout more all service</Text>
              <View style={{width:'100%', height:175}}>
                <ScrollView horizontal={true} pagingEnabled={true} style={{width:'100%', height:'75%', flexDirection:'row'}}>
                  <View style={{width:Dimensions.get('window').width, height:'100%', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("IndikosSearch")}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indikos.png')}/>
                      <Text style={{fontSize:12}}>Indikos</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("IndineedsSearch")}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indineed.png')} />
                      <Text style={{fontSize:12}}>Indineed</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("Indinews")}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indinews.png')}  />
                      <Text style={{fontSize:12}}>Indinews</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indipulsa.png')} />
                      <Text style={{fontSize:12}}>Indipulsa</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:Dimensions.get('window').width, height:'100%', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indipick.png')} />
                      <Text style={{fontSize:12}}>Indipick</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indievent.png')} />
                      <Text style={{fontSize:12}}>Indievent</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indicareer.png')} />
                      <Text style={{fontSize:12}}>Indicareer</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/menu/indilaundry.png')} />
                      <Text style={{fontSize:12}}>Indilaundry</Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
                <View style={{width:'100%', height:'25%', alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                  <FontAwesome name={"circle"} size={8} color={"blue"} style={{marginLeft:5}}/>
                  <FontAwesome name={"circle"} size={8} color={"skyblue"} style={{marginLeft:5}}/>
                </View>
              </View>
            </View>
          </View>
          <View style={{backgroundColor:'white', marginTop:20}}>
            <View style={{height:50, width:'100%', flexDirection:'row'}}>
              <View style={{width:'75%', height:'100%', alignItems:'center', flexDirection:'row', paddingLeft:20}}>
                <Image style={{width:'15%', height:'40%', resizeMode:'contain'}} source={require('../../assets/img/home/crown.png')}/>
                <Text style={{fontSize:18, fontWeight:'bold', color:'black', marginLeft:5}}>Best selling</Text>
              </View>
              <View style={{width:'25%', height:'100%', justifyContent:'center', alignItems:'flex-end', paddingRight:20}}>
                <TouchableOpacity>
                  <Text style={{color:'#65c5f2'}} onPress={()=>this.props.navigation.navigate("KosanList")}>View all</Text> 
                </TouchableOpacity>
              </View>
            </View>
            <ScrollView horizontal={true} style={{height:225, width:'100%', flexDirection:'row', marginTop:10, marginBottom:10}}>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("KosanDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("KosanDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-2.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("KosanDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-2.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={{backgroundColor:'white', marginTop:20}}>
            <View style={{height:50, width:'100%', flexDirection:'row'}}>
              <View style={{width:'75%', height:'100%', alignItems:'center', flexDirection:'row', paddingLeft:20}}>
                <Image style={{width:'15%', height:'40%', resizeMode:'contain'}} source={require('../../assets/img/home/indinews.png')}/>
                <Text style={{fontSize:18, fontWeight:'bold', color:'black', marginLeft:5}}>News Room</Text>
              </View>
              <View style={{width:'25%', height:'100%', justifyContent:'center', alignItems:'flex-end', paddingRight:20}}>
              </View>
            </View>
            <View>
              <Listnews
                title="Liburan versi low budget"
                headline="Liburan di tahun baru memang waktu yang pas. Selain banyak promosi ..."
                time="6"
                bookmark={true}
                like={false}
                img={require('../../assets/img/indinews/banner-1.png')}
                navigate={this.props.navigation.navigate}
              />
              <Listnews
                title="Merantau? Gausah galau"
                headline="Apa yang pertama kali kalian pikirkan tentang merantau ? Kebanyakan ..."
                time="40"
                bookmark={false}
                like={true}
                img={require('../../assets/img/indinews/banner-2.png')}
                navigate={this.props.navigation.navigate}
              />
              <Listnews
                title="Berkenalan dengan AIDS"
                headline="Kata AIDS selalu diidentikan dengan HIV. Padahal, kedua hal tersebut ..."
                time="50"
                bookmark={false}
                like={false}
                img={require('../../assets/img/indinews/banner-3.png')}
                navigate={this.props.navigation.navigate}
              />
              {/*<TouchableOpacity style={{height:175, flexDirection:'row', paddingBottom:10, paddingTop:10, borderColor:'lightgray', borderBottomWidth:1}}>
                <View style={{width:'37.5%', height:'100%',padding:10}}>
                  <View style={{backgroundColor:'black', height:'100%', width:'100%', borderRadius:5}}></View>
                </View>
                <View style={{width:'62.5%', height:'100%', paddingTop:10, paddingBottom:10}}>
                  <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10}}>
                    <Text style={{fontWeight:'bold', color:'black'}}>Liburan versi low budget</Text>
                    <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'15%', height:'100%'}}>
                      <Image style={{width:'60%', height:'60%', resizeMode:'contain'}} source={require('../../assets/img/home/bookmark.png')}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:'100%', height:'50%', paddingRight:10}}>
                    <Text style={{fontSize:12}}>Liburan di tahun baru memang waktu yang pas. Selain banyak promosi ...</Text>
                  </View>
                  <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10 }}>
                    <View style={{alignItems:'center', flexDirection:'row'}}>
                      <TouchableOpacity>
                        <MaterialIcons name={"favorite-border"} size={25} color={"gray"}/>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <FontAwesome name={"commenting-o"} size={25} color={"gray"} style={{marginLeft:10}}/>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <Entypo name={"share"} size={25} color={"gray"} style={{marginLeft:10}}/>
                      </TouchableOpacity>
                    </View>
                      <Text style={{fontSize:12}}>6m ago</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{height:175, flexDirection:'row', paddingBottom:10, paddingTop:10, borderColor:'lightgray', borderBottomWidth:1}}>
                <View style={{width:'37.5%', height:'100%',padding:10}}>
                  <View style={{backgroundColor:'red', height:'100%', width:'100%', borderRadius:5}}></View>
                </View>
                <View style={{width:'62.5%', height:'100%', paddingTop:10, paddingBottom:10}}>
                  <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10}}>
                    <Text style={{fontWeight:'bold', color:'black'}}>Merantau? Gausah galau</Text>
                    <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'15%', height:'100%'}}>
                      <Image style={{width:'60%', height:'60%', resizeMode:'contain'}} source={require('../../assets/img/home/bookmark.png')}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:'100%', height:'50%', paddingRight:10}}>
                    <Text style={{fontSize:12}}>Apa yang pertama kali kalian pikirkan tentang merantau ? Kebanyakan ...</Text>
                  </View>
                  <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10 }}>
                    <View style={{alignItems:'center', flexDirection:'row'}}>
                      <TouchableOpacity>
                        <MaterialIcons name={"favorite-border"} size={25} color={"gray"}/>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <FontAwesome name={"commenting-o"} size={25} color={"gray"} style={{marginLeft:10}}/>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <Entypo name={"share"} size={25} color={"gray"} style={{marginLeft:10}}/>
                      </TouchableOpacity>
                    </View>
                      <Text style={{fontSize:12}}>40m ago</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{height:175, flexDirection:'row', paddingBottom:10, paddingTop:10, borderColor:'lightgray', borderBottomWidth:1}}>
                <View style={{width:'37.5%', height:'100%',padding:10}}>
                  <View style={{backgroundColor:'yellow', height:'100%', width:'100%', borderRadius:5}}></View>
                </View>
                <View style={{width:'62.5%', height:'100%', paddingTop:10, paddingBottom:10}}>
                  <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10}}>
                    <Text style={{fontWeight:'bold', color:'black'}}>Berkenalan dengan AIDS</Text>
                    <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'15%', height:'100%'}}>
                      <Image style={{width:'60%', height:'60%', resizeMode:'contain'}} source={require('../../assets/img/home/bookmark.png')}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:'100%', height:'50%', paddingRight:10}}>
                    <Text style={{fontSize:12}}>Kata AIDS selalu diidentikan dengan HIV. Padahal, kedua hal tersebut ...</Text>
                  </View>
                  <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10 }}>
                    <View style={{alignItems:'center', flexDirection:'row'}}>
                      <TouchableOpacity>
                        <MaterialIcons name={"favorite-border"} size={25} color={"gray"}/>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <FontAwesome name={"commenting-o"} size={25} color={"gray"} style={{marginLeft:10}}/>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <Entypo name={"share"} size={25} color={"gray"} style={{marginLeft:10}}/>
                      </TouchableOpacity>
                    </View>
                      <Text style={{fontSize:12}}>50m ago</Text>
                  </View>
                </View>
              </TouchableOpacity>*/}
            </View>
          </View>
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username,password) => dispatch(login(username,password)),
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Home);
