import React, {Component} from 'react';
import {View, Image, TouchableOpacity, ScrollView, Dimensions} from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, CardItem, Text, Body, Right, Picker, Title, Tab, Tabs, ScrollableTab } from 'native-base';

export default class All extends Component {
  constructor(props) {
    super(props);
    this.state = {
      like: this.props.like,
      isVisible: false
    };
  }

  render() {
    return (
      <Container>
        <Content>
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', margin:10}}>
            <Button bordered info>
              <Text style={{fontSize:14, fontWeight:'600'}}> Remove All </Text>
            </Button>
            <View style={{flexDirection:'row', alignItems:'center'}}>
              <Text style={{fontSize:14, fontWeight:'500', color:'black'}}>Select
                <Text style={{fontSize:14, fontWeight:'300', color:'gray'}}> (4/4) </Text>
              </Text>
              <TouchableOpacity style={{marginLeft:5}}>
                <Icon type="FontAwesome" name="check-square" style={{color:'#65c5f2'}} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{width:'100%', borderColor:'lightgray', borderWidth:0.5, marginTop:5, marginBottom:20}} />
          <Card style={{flexDirection:'row', padding:5, margin:10, borderRadius:5, width:'95%', alignSelf:'center'}}>
            <Card style={{width:'30%', borderRadius:10}}>
              <Image style={{height:100, width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/food1.jpg")}/>
            </Card>
            <View style={{margin:10, width:'70%'}}>
              <Text style={{marginTop:5, fontSize:14, fontWeight:'500'}}>Al contrario di pensi quuanto ...</Text>
              <Text numberOfLines={1} style={{marginTop:10, fontSize:12, color:'gray', width:200}}>Jl. Liburan di tahun baru memang menyenangkan Jl. Liburan di tahun baru memang menyenangkan</Text>
              <View style={{flexDirection:'row', marginTop:15, alignItems:'center'}}>
                <Text style={{fontSize:16, color:'#65c5f2', fontWeight:'600'}}>RM 200</Text>
              </View>
              <TouchableOpacity style={{position:'absolute', top:-5, right:15}}>
                <Icon type="FontAwesome" name="check-square" style={{color:'#65c5f2', fontSize:20}} />
              </TouchableOpacity>
              <TouchableOpacity style={{position:'absolute', bottom:-5, right:15}}>
                <Icon type="FontAwesome" name="trash" style={{color:'lightgray', fontSize:20}} />
              </TouchableOpacity>
            </View>
          </Card>
          <Card style={{flexDirection:'row', padding:5, margin:10, borderRadius:5, width:'95%', alignSelf:'center'}}>
            <Card style={{width:'30%', borderRadius:10}}>
              <Image style={{height:100, width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/food1.jpg")}/>
            </Card>
            <View style={{margin:10, width:'70%'}}>
              <Text style={{marginTop:5, fontSize:14, fontWeight:'500'}}>Al contrario di pensi quuanto ...</Text>
              <Text numberOfLines={1} style={{marginTop:10, fontSize:12, color:'gray', width:200}}>Jl. Liburan di tahun baru memang menyenangkan Jl. Liburan di tahun baru memang menyenangkan</Text>
              <View style={{flexDirection:'row', marginTop:15, alignItems:'center'}}>
                <Text style={{fontSize:16, color:'#65c5f2', fontWeight:'600'}}>RM 120</Text>
              </View>
              <TouchableOpacity style={{position:'absolute', top:-5, right:15}}>
                <Icon type="FontAwesome" name="check-square" style={{color:'#65c5f2', fontSize:20}} />
              </TouchableOpacity>
              <TouchableOpacity style={{position:'absolute', bottom:-5, right:15}}>
                <Icon type="FontAwesome" name="trash" style={{color:'lightgray', fontSize:20}} />
              </TouchableOpacity>
            </View>
          </Card>
          <Card style={{flexDirection:'row', padding:5, margin:10, borderRadius:5, width:'95%', alignSelf:'center'}}>
            <Card style={{width:'30%', borderRadius:10}}>
              <Image style={{height:100, width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/food1.jpg")}/>
            </Card>
            <View style={{margin:10, width:'70%'}}>
              <Text style={{marginTop:5, fontSize:14, fontWeight:'500'}}>Al contrario di pensi quuanto ...</Text>
              <Text numberOfLines={1} style={{marginTop:10, fontSize:12, color:'gray', width:200}}>Jl. Liburan di tahun baru memang menyenangkan Jl. Liburan di tahun baru memang menyenangkan</Text>
              <View style={{flexDirection:'row', marginTop:15, alignItems:'center'}}>
                <Text style={{fontSize:16, color:'#65c5f2', fontWeight:'600'}}>RM 90</Text>
                
              </View>
              <TouchableOpacity style={{position:'absolute', top:-5, right:15}}>
                <Icon type="FontAwesome" name="check-square" style={{color:'#65c5f2', fontSize:20}} />
              </TouchableOpacity>
              <TouchableOpacity style={{position:'absolute', bottom:-5, right:15}}>
                <Icon type="FontAwesome" name="trash" style={{color:'lightgray', fontSize:20}} />
              </TouchableOpacity>
            </View>
          </Card>
          <Card style={{flexDirection:'row', padding:5, margin:10, borderRadius:5, width:'95%', alignSelf:'center', marginBottom:20}}>
            <Card style={{width:'30%', borderRadius:10}}>
              <Image style={{height:100, width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/food2.jpg")}/>
            </Card>
            <View style={{margin:10, width:'70%'}}>
              <Text style={{marginTop:5, fontSize:14, fontWeight:'500'}}>Al contrario di pensi quuanto ...</Text>
              <Text numberOfLines={1} style={{marginTop:10, fontSize:12, color:'gray', width:200}}>Jl. Liburan di tahun baru memang menyenangkan Jl. Liburan di tahun baru memang menyenangkan</Text>
              <View style={{flexDirection:'row', marginTop:15, alignItems:'center'}}>
                <Text style={{fontSize:16, color:'#65c5f2', fontWeight:'600'}}>RM 98</Text>
              </View>
              <TouchableOpacity style={{position:'absolute', top:-5, right:15}}>
                <Icon type="FontAwesome" name="check-square" style={{color:'#65c5f2', fontSize:20}} />
              </TouchableOpacity>
              <TouchableOpacity style={{position:'absolute', bottom:-5, right:15}}>
                <Icon type="FontAwesome" name="trash" style={{color:'lightgray', fontSize:20}} />
              </TouchableOpacity>
            </View>
          </Card>
          <View style={{backgroundColor:'rgba(242, 247, 255,0.5)', height:20}}></View>
          {/*<Card style={{ marginTop:0}}>
            <CardItem style={{backgroundColor:'white', marginTop:20}}>
              <Text style={{fontWeight:'500', fontSize:18, color:'black'}}>Rekomendasi buat kamu</Text>
            </CardItem>
            <CardItem cardBody style={{height:300, paddingBottom:30, paddingTop:10, backgroundColor:'white'}}>
              <ScrollView horizontal={true} style={{height:270, width:'100%', flexDirection:'row', paddingLeft:15, paddingRight:15}}>
                <View style={{borderRadius:10, marginRight:15}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <CardItem cardBody style={{height: '60%', width:'95%'}}>
                      <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/home/best-selling-1.png")}/>
                      <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                        <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                      </TouchableOpacity>
                    </CardItem>
                    <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                      <Icon type="FontAwesome" name="heart" style={{fontSize:12, color:"gray", width:12, marginTop:5}}/>
                      <Text style={{color:'gray', marginLeft:5, fontSize:12, fontWeight:'400'}}>124 Likes</Text>
                    </View>
                    <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Sekai Rice Cooker magic</Text>
                    <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>Rp. 156.300</Text>
                  </TouchableOpacity>
                </View>
                <View style={{borderRadius:10, marginRight:15}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <CardItem cardBody style={{height: '60%', width:'95%'}}>
                      <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/home/best-selling-1.png")}/>
                      <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                        <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                      </TouchableOpacity>
                    </CardItem>
                    <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                      <Icon type="FontAwesome" name="heart" style={{fontSize:12, color:"gray", width:12, marginTop:5}}/>
                      <Text style={{color:'gray', marginLeft:5, fontSize:12, fontWeight:'400'}}>124 Likes</Text>
                    </View>
                    <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Sneaker ORIGINAL Imperium</Text>
                    <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>Rp. 116.100</Text>
                  </TouchableOpacity>
                </View>
                <View style={{borderRadius:10, marginRight:15}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <CardItem cardBody style={{height: '60%', width:'95%'}}>
                      <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/home/best-selling-1.png")}/>
                      <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                        <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                      </TouchableOpacity>
                    </CardItem>
                    <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                      <Icon type="FontAwesome" name="heart" style={{fontSize:12, color:"gray", width:12, marginTop:5}}/>
                      <Text style={{color:'gray', marginLeft:5, fontSize:12, fontWeight:'400'}}>124 Likes</Text>
                    </View>
                    <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Sekai Rice Cooker magic</Text>
                    <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>Rp. 156.300</Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </CardItem>
          </Card>*/}
        </Content>
        <Card style={{flexDirection:'row', marginBottom:0, padding:10, justifyContent:'space-between', alignItems:'center'}}>
          <View style={{marginLeft:5}}>
            <Text style={{fontSize:12, color:'#65c5f2'}}>Total Price</Text>
            <Text style={{marginTop:5, color:'#65c5f2', fontWeight:'bold'}}>RM 800</Text>
          </View>
          <Button info>
            <Text style={{fontSize:14, fontWeight:'bold', marginLeft:10, marginRight:10}}> BUY NOW </Text>
          </Button>
        </Card>
      </Container>
    );
  }
}
