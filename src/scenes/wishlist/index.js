import React, {Component} from 'react';
import { Image, View } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, Text, Body, Right, Picker, Title, Tab, Tabs, TabHeading, ScrollableTab } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';
import All from './all';
import Indikos from './indikos';
import Indineeds from './indineeds';

class Wishlist extends Component {
  static navigationOptions = {
    header: null ,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <Container>
        <Header hasTabs style={{backgroundColor:'white'}}>
          <Left style={{marginLeft:5}}>
            <Button transparent onPress={()=>this.props.navigation.goBack()}>
              <Icon name='arrow-back' style={{color:'black'}} />
            </Button>
          </Left>
          <Body>
            <Title style={{color:'black', fontWeight:'600'}}>Wishlist</Title>
          </Body>
          <Right>
          </Right>
        </Header>
        <Tabs renderTabBar={()=> <ScrollableTab style={{backgroundColor:'white'}} underlineStyle={{backgroundColor:'#65c5f2'}} />}>
          <Tab heading={ <TabHeading style={{backgroundColor:'white'}}><Text style={{color:'#65c5f2'}}>All</Text></TabHeading> }>
            <All />
          </Tab>
          <Tab heading={ <TabHeading style={{backgroundColor:'white'}}><Text style={{color:'#65c5f2'}}>Food</Text></TabHeading> }>
            <Indikos />
          </Tab>
          <Tab heading={ <TabHeading style={{backgroundColor:'white'}}><Text style={{color:'#65c5f2'}}>Drinks</Text></TabHeading> }>
            <Indineeds />
          </Tab>
        </Tabs>
        <Maintabs navigate={this.props.navigation.navigate} />
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Wishlist);
