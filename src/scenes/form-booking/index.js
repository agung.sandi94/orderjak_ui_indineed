import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Body, Right, Title, Button, Icon, Card, CardItem, Text, Picker } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';
import DatePicker from 'react-native-datepicker'

class FormBooking extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      kota : "",
      lama : "bulan",
      date : "2016-05-15"
    };
  }

  onKotaChange(value: string) {
    this.setState({
      kota: value
    });
  }

  onLamaChange(value: string) {
    this.setState({
      lama: value
    });
  }

  render() {
    return (
      <Container>
        <Header transparent>
          <Left style={{marginLeft:5}}>
            <Button transparent onPress={()=>{this.props.navigation.goBack()}}>
              <Icon name='md-arrow-back' style={{color:'black', fontSize:28}} />
            </Button>
          </Left>
          <Body>
          </Body>
          <Right>
          </Right>
        </Header>
        <Content style={{padding:20, paddingTop:0}}>
          <Text style={{fontSize:22, fontWeight:'bold', marginBottom:10}}>Form Booking</Text>
          <Item stackedLabel>
            <Label style={{fontSize:12, color:'black', marginTop:10}}>Nama Lengkap *</Label>
            <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder={"Nama Lengkap"} />
          </Item>
          <Item stackedLabel>
            <Label style={{fontSize:12, color:'black', marginTop:10}}>E-mail *</Label>
            <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder={"Indikos@gmail.com"} />
          </Item>
          <Item stackedLabel>
            <Label style={{fontSize:12, color:'black', marginTop:10}}>Alamat *</Label>
            <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, height:100, width:'100%'}} multiline={true} />
          </Item>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{width:'49%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, color:'black', marginTop:10}}>Kota</Label>
                <Item picker style={{width:'100%', fontSize:12, marginTop:5}}>
                  <Picker
                    mode="dropdown"
                    style={{width:'50%', borderColor:'lightgray', borderWidth:0.5}}
                    textStyle={{ color: (this.state.kota=="")?"gray":"black", fontSize:12 }}
                    selectedValue={this.state.kota}
                    onValueChange={this.onKotaChange.bind(this)}
                  >
                    <Picker.Item label="Pilih Kota" value="" />
                    <Picker.Item label="Jakarta" value="jakarta" />
                    <Picker.Item label="Bandung" value="bandung" />
                    <Picker.Item label="Semarang" value="semarang" />
                    <Picker.Item label="Surabaya" value="surabaya" />
                    <Picker.Item label="Yogya" value="yogya" />
                  </Picker>
                </Item>
              </Item>
            </View>
            <View style={{width:'49%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, color:'black', marginTop:10}}>Kode Pos *</Label>
                <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} />
              </Item>
            </View>
          </View>
          <Item stackedLabel>
            <Label style={{fontSize:12, color:'black', marginTop:10}}>Nomor Telepon *</Label>
            <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder="+62 852 2171 1234" />
          </Item>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{width:'30%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, color:'black', marginTop:10}}>Lama Waktu</Label>
                <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder="1"/>
              </Item>
            </View>
            <View style={{width:'68%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, color:'black', marginTop:10}}></Label>
                <Item picker style={{width:'100%', fontSize:12, marginTop:5}}>
                  <Picker
                    mode="dropdown"
                    style={{width:'70%', borderColor:'lightgray', borderWidth:0.5}}
                    textStyle={{ color: (this.state.lama=="")?"gray":"black", fontSize:12 }}
                    selectedValue={this.state.lama}
                    onValueChange={this.onLamaChange.bind(this)}
                  >
                    <Picker.Item label="Bulan" value="bulan" />
                    <Picker.Item label="Tahun" value="tahun" />
                    <Picker.Item label="Hari" value="hari" />
                  </Picker>
                </Item>
              </Item>
            </View>
          </View>
          <Label style={{fontSize:12, color:'black', marginTop:10}}>Tentukan Tanggal dan Waktu</Label>
          <View style={{flexDirection:'row', marginTop:5}}>
            <View style={{width:'50%'}}>
              <Icon type="Foundation" name='calendar' style={{color:'lightgray', fontSize:24, position:'absolute', top: 7, left:10}} />
              <DatePicker
                style={{width: '95%'}}
                date={this.state.date}
                mode="date"
                placeholder="select date"
                format="DD-MM-YYYY"
                minDate="2018-01-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                onDateChange={(date) => {this.setState({date: date})}}
              />
              <Icon type="Ionicons" name='ios-arrow-down' style={{color:'lightgray', fontSize:20, position:'absolute', top: 10, right:20}} />
            </View>
            <View style={{width:'50%', alignItems:'flex-end'}}>
              <Icon type="Ionicons" name='md-time' style={{color:'lightgray', fontSize:24, position:'absolute', top: 7, left:20}} />
              <DatePicker
                style={{width: '95%'}}
                date={this.state.time}
                mode="time"
                placeholder="select time"
                format="hh:mm"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                is24Hour={true}
                allowFontScaling={false}
                showIcon={false}
                onDateChange={(time) => {this.setState({time: time})}}
              />
              <Icon type="Ionicons" name='ios-arrow-down' style={{color:'lightgray', fontSize:20, position:'absolute', top: 10, right:10}} />
            </View>
          </View>
          <Button onPress={()=>{this.props.navigation.navigate("ProsesPembayaran")}} info style={{marginTop:30, marginBottom:20, width:'100%', alignItems:'center', justifyContent:'center'}}>
            <Text style={{fontWeight:'600', fontSize:14}}>Booking Kosan</Text>
          </Button>
        </Content>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(FormBooking);
