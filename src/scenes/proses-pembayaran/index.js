import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, CardItem, Text, Body, Right, Picker } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';

class ProsesPembayaran  extends Component {
  static navigationOptions = {
    title: "Proses Pembayaran",
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff'}}>
          <Card>
            <CardItem style={{marginTop:-10}}>
              <Text style={{fontSize:14, marginTop:10, color:'gray'}}>Reservasi dengan kode</Text>
            </CardItem>
            <CardItem style={{justifyContent:'space-between', alignItems:'center', marginBottom:15, marginTop:-10}}>
              <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                <Icon type="FontAwesome" name='qrcode' style={{color:'#65c5f2', fontSize:18, width:18}} />
                <Text style={{fontSize:16, fontWeight:'bold', color:'black', marginLeft:10}}>KR1020</Text>
              </View>
              <TouchableOpacity>
                <Text style={{fontSize:14, color:'#65c5f2', textDecorationLine:'underline', fontWeight:'600'}} >Dikonfirmasi</Text>
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Text style={{alignSelf:'center', textAlign:'center', fontSize:14, margin:30, lineHeight:20, color:'gray'}}>Mohon melakukan pembayaran ke rekening {'\n'} di bawah ini :</Text>
          <Card style={{marginRight:20, marginLeft:20}}>
            <CardItem style={{borderColor:'lightgray', borderBottomWidth:0.5, justifyContent:'space-between', alignItems:'center'}}>
              <Image style={{height:50, width:120, resizeMode:'stretch'}} source={require('../../assets/img/logo-mandiri.jpg')} />
              <View style={{alignItems:'flex-end'}}>
                <Text style={{fontSize:12, color:'gray'}}>Bank Mandiri</Text>
                <Text style={{marginTop:5, fontSize:14, fontWeight:'600'}}>132-00-2141316-7</Text>
              </View>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={{alignSelf:'center', fontSize:14, color:'gray', marginTop:15}}>Jumlah:</Text>
                <View style={{flexDirection:'row', alignSelf:'center', margin:15, alignItems:'center'}}>
                  <Text style={{fontSize:24, color:'#65c5f2', fontWeight:'bold'}}>Rp 1.500.000</Text>
                  <Icon type="FontAwesome" name='check-circle' style={{color:'orange', fontSize:16, marginLeft:10}} />
                </View>
              </Body>
            </CardItem>
            <CardItem style={{borderColor:'lightgray', borderTopWidth:0.5, alignItems:'center', justifyContent:'center'}}>
              <Text style={{alignSelf:'center', fontSize:14, color:'black', marginTop:10, marginBottom:10}}>PT Helmpy Malindo Makmur</Text>
            </CardItem>
         </Card>
          <Text style={{alignSelf:'center', textAlign:'center', fontSize:14, margin:30, lineHeight:20, color:'gray'}}>Atau, sudah melakukan pembayaran ?</Text>
          <Button primary style={{alignSelf:'center', width:'80%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2', marginBottom:20}}>
            <Text style={{fontSize:14, fontWeight:'bold'}}>Konfirmasi Pembayaran</Text>
          </Button>
        </Content>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(ProsesPembayaran);
