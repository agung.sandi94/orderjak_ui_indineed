import React, {Component} from 'react';
import { Image } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, Text } from 'native-base';
import { connect } from 'react-redux';

class ForgotPassword  extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff'}}>
          <Button transparent style={{ position:'absolute', top:40, left: 10}} onPress={()=>this.props.navigation.goBack()}>
            <Icon name='arrow-back' style={{color:'gray', fontSize:30}} />
          </Button>
          <Image style={{height:150, width:150, resizeMode:'contain', alignSelf:'center', marginTop: 75}} source={require('../../assets/img/logo.png')} />
          <Text style={{alignSelf:'center', fontSize:25, color:'#65c5f2', fontWeight:'bold', marginTop:20}}>Forgot Password</Text>
          <Text style={{alignSelf:'center', textAlign:'center', width:'80%', fontWeight:'500', fontSize:14, marginTop:20, color:'black'}}>Please enter your email / mobile & we will send an OTP number.</Text>
          <Form style={{padding:20, marginTop:20}}>
            <Card>
              <Item regular style={{marginLeft:10, marginRight:10, borderWidth:0, borderColor:'transparent'}}>
                <Icon active name='mail' style={{color:'#65c5f2', marginLeft:5}} />
                <Input placeholder='Email / mobile'/>
              </Item>
            </Card>
          </Form>
          <Button rounded style={{alignSelf:'center', width:120, justifyContent:'center', marginTop:50}}>
            <Text style={{color:'white', fontWeight:'bold'}}>Send</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(ForgotPassword);
