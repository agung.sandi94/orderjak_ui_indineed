import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Body, Right, Title, Button, Icon, Card, CardItem, Text } from 'native-base';
import { connect } from 'react-redux';
import EmptyBasket from '../../components/empty-basket';
import Maintabs from '../../components/maintabs-new';

class IndineedsBasket  extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this._setHidePopUp = this._setHidePopUp.bind(this);
    this.changeTotal = this.changeTotal.bind(this);
    this.state = {
      onClose: this._setHidePopUp,
      isEmpty: true,
      notes: "",
      total: 1
    };
  }

  _setHidePopUp() {
    this.setState({ isEmpty: false });
  }

  changeTotal(act){
    var total = this.state.total
    if (act == "minus"){
      if (total > 1){
        total = total - 1
        this.setState({total})
      }
    } else {
      total = total + 1
      this.setState({total})
    }
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'white'}}>
          <Left>
            <Button transparent onPress={()=>{this.props.navigation.goBack()}}>
              <Icon name='arrow-back' style={{color:'black'}} />
            </Button>
          </Left>
          <Body>
            <Title style={{color:'black'}}>Add to Basket</Title>
          </Body>
          <Right>
            <Button transparent onPress={()=>{this.setState({isEmpty:true})}}>
              <Icon type="FontAwesome5" name='shopping-basket' style={{color:'#65c5f2', fontSize:20}}/>
            </Button>
          </Right>
        </Header>
        <Content style={{padding:20}}>
          <Card>
            <View style={{flexDirection:'row'}}>
              <Image style={{height:'100%', width:135, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
              <View style={{flex:1}}>
                <View style={{flexDirection:'row', justifyContent:'space-between', padding:10}}>
                  <Text style={{fontSize:12, fontWeight:'600', width:'85%'}}>Reuben Sandwich</Text>
                  <Icon type="MaterialIcons" name='more-vert' style={{color:'gray', fontSize:16, width:20}}/>
                </View>
                <View style={{flexDirection:'row', padding:10, alignItems:'center', justifyContent:'space-between'}}>
                  <Text style={{fontSize:12, fontWeight:'600', color:'gray'}}>Total</Text>
                  <View style={{flexDirection:'row', borderRadius:15, borderColor:'lightgray', borderWidth:0.5, alignItems:'center', justifyContent:'center', marginRight:10}}>
                    <TouchableOpacity onPress={()=>{this.changeTotal('minus')}} style={{width:20, height:20, backgroundColor:'#ededed', alignItems:'center', justifyContent:'center', borderTopLeftRadius:15, borderBottomLeftRadius:15}}>
                      <Icon type="Feather" name='minus' style={{color:'gray', fontSize:12}}/>
                    </TouchableOpacity>
                    <Text style={{width:65, textAlign:'center', fontSize:12}}>{this.state.total}</Text>
                    <TouchableOpacity onPress={()=>{this.changeTotal('add')}} style={{width:20, height:20, backgroundColor:'#ededed', alignItems:'center', justifyContent:'center', borderTopRightRadius:15, borderBottomRightRadius:15}}>
                      <Icon type="Feather" name='plus' style={{color:'gray', fontSize:12}}/>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{width:'100%', marginTop:6, backgroundColor:'#65c5f2', justifyContent:'center', alignItems:'center', padding:10, borderBottomRightRadius:3}}>
                  <Text style={{fontSize:16, fontWeight:'bold', color:'white'}}>RM 500</Text>
                </View>
              </View>
            </View>
          </Card>
          <Text style={{marginTop:40, fontSize:12, fontWeight:'600'}}>Add Notes For Orders</Text>
          <Item fixedLabel>
            <Input value={this.state.notes} onChangeText={(notes)=>this.setState({notes})} maxLength={144}/>
          </Item>
          <Text style={{alignSelf:'flex-end', marginTop:10, fontSize:12, color:'gray'}}>{this.state.notes.length} / 144</Text>
        </Content>
        <EmptyBasket
          visible={this.state.isEmpty}
          onClose={this.state.onClose}
          navigation={this.props.navigation} />
        <Card style={{marginBottom:0}}>
          <CardItem style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View>
              <Text style={{fontSize:12, color:'gray'}}>Product Price</Text>
              <Text style={{color:'#65c5f2', fontWeight:'bold'}}>RM 500</Text>
            </View>
            <Button style={{backgroundColor:'#65c5f2', width:125, alignItems:'center', justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("Checkout")}>
              <Text style={{fontSize:12, color:'white', fontWeight:'bold'}}>Buy Now</Text>
            </Button>
          </CardItem>
        </Card>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(IndineedsBasket);
