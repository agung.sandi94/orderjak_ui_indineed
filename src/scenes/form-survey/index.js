import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Body, Right, Title, Button, Icon, Card, CardItem, Text, Picker, ListItem, CheckBox } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';
import DatePicker from 'react-native-datepicker'

class FormBooking extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      gender : "Laki-laki",
      lama : "bulan",
      lahir : "2016-05-15",
      tnc: false
    };
  }

  onGenderChange(value: string) {
    this.setState({
      gender: value
    });
  }

  onLamaChange(value: string) {
    this.setState({
      lama: value
    });
  }

  render() {
    return (
      <Container>
        <Header transparent>
          <Left style={{marginLeft:5}}>
            <Button transparent onPress={()=>{this.props.navigation.goBack()}}>
              <Icon name='md-arrow-back' style={{color:'black', fontSize:28}} />
            </Button>
          </Left>
          <Body>
          </Body>
          <Right>
          </Right>
        </Header>
        <Content style={{padding:20, paddingTop:0}}>
          <View style={{marginLeft:-20, marginRight:-20, padding:20, backgroundColor:'yellow', alignItems:'center', justifyContent:'center'}}>
            <Text style={{textAlign:'center', fontSize:14, lineHeight:20, width:'85%'}}>
              Langsung memilih kosan
              <Text style={{fontWeight:'bold', fontSize:14}}> tanpa melakukan survey </Text>
              , bisa langsung hubungi CS kami di
              <Text style={{fontWeight:'bold', fontSize:14}}> 081312656465 </Text>
            </Text>
          </View>
          <Text style={{fontSize:22, fontWeight:'bold', marginBottom:10, marginTop:20}}>Form Survey</Text>
          <Item stackedLabel>
            <Label style={{fontSize:12, color:'black', marginTop:10}}>Nama Lengkap *</Label>
            <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder={"Nama Lengkap"} />
          </Item>
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <View style={{width:'49%'}}>
              <Label style={{fontSize:12, color:'black', marginTop:10}}>Jenis Kelamin *</Label>
              <Item picker style={{width:'100%', fontSize:12, marginTop:5}}>
                <Picker
                  mode="dropdown"
                  style={{width:'50%', borderColor:'lightgray', borderWidth:0.5, height:40}}
                  textStyle={{ color: "black", fontSize:12 }}
                  selectedValue={this.state.gender}
                  onValueChange={this.onGenderChange.bind(this)}
                >
                  <Picker.Item label="Laki-laki" value="Laki-laki" />
                  <Picker.Item label="Wanita" value="wanita" />
                </Picker>
              </Item>
            </View>
            <View style={{width:'49%'}}>
              <Label style={{fontSize:12, color:'black', marginTop:10}}>Tanggal Lahir *</Label>
              <View style={{width:'100%', marginTop:5}}>
                <Icon type="Foundation" name='calendar' style={{color:'lightgray', fontSize:24, position:'absolute', top: 7, left:10}} />
                <Icon type="Ionicons" name='ios-arrow-down' style={{color:'lightgray', fontSize:20, position:'absolute', top: 10, right:10}} />
                <DatePicker
                  style={{width: '100%'}}
                  date={this.state.lahir}
                  mode="date"
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="2018-01-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  onDateChange={(lahir) => {this.setState({lahir: lahir})}}
                />
              </View>
            </View>
          </View>
          <Item stackedLabel>
            <Label style={{fontSize:12, color:'black', marginTop:10}}>E-mail *</Label>
            <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder={"Indikos@gmail.com"} />
          </Item>

          <Item stackedLabel>
            <Label style={{fontSize:12, color:'black', marginTop:10}}>Nomor Telepon *</Label>
            <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder="+62 852 2171 1234" />
          </Item>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View style={{width:'30%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, color:'black', marginTop:10}}>Lama Waktu</Label>
                <Input style={{fontSize:12, borderColor:'lightgray', borderWidth:0.5, paddingLeft:10, paddingRight:10, marginTop:5, width:'100%'}} placeholder="1"/>
              </Item>
            </View>
            <View style={{width:'68%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, color:'black', marginTop:10}}></Label>
                <Item picker style={{width:'100%', fontSize:12, marginTop:5}}>
                  <Picker
                    mode="dropdown"
                    style={{width:'70%', borderColor:'lightgray', borderWidth:0.5}}
                    textStyle={{ color: "gray", fontSize:12 }}
                    selectedValue={this.state.lama}
                    onValueChange={this.onLamaChange.bind(this)}
                  >
                    <Picker.Item label="Bulan" value="bulan" />
                    <Picker.Item label="Tahun" value="tahun" />
                    <Picker.Item label="Hari" value="hari" />
                  </Picker>
                </Item>
              </Item>
            </View>
          </View>
          <View style={{marginTop:25, flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>{this.setState({tnc:!this.state.tnc})}}>
              <Icon type="FontAwesome" name={(this.state.tnc)?'check-square-o':'square-o'} style={{color:(this.state.tnc)?'#65c5f2':'lightgray', fontSize:16}} />
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'gray', marginLeft:8, fontWeight:'500'}}>Saya menyatakan serius ingin survey Kost pada jadwal tersebut</Text>
          </View>
          <Button info style={{marginTop:30, marginBottom:20, width:'100%', alignItems:'center', justifyContent:'center'}}>
            <Text style={{fontWeight:'600', fontSize:14}}>Permintaan Survey</Text>
          </Button>
        </Content>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(FormBooking);
