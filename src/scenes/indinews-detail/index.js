import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';

class IndinewsDetail extends Component {
  static navigationOptions = ({navigation}) => ({
    title: "Indinews" ,
    headerBackTitle: 'Back',
    headerRight: <TouchableOpacity style={{marginRight:15}} onPress={()=>{navigation.state.params._bookmark()}}>
                    <Image style={{width:25, height:25, resizeMode:'contain'}} source={(navigation.state.params.bookmark)?require("../../assets/img/home/bookmark-active.png"):require("../../assets/img/home/bookmark.png")} />
                 </TouchableOpacity>
  });

  constructor(props) {
    super(props);
    this.state={
      bookmark: this.props.navigation.state.params.bookmark
    }
  }

  componentWillMount(){
    this.props.navigation.setParams({
      _bookmark: this._bookmark.bind(this),
    });
  }

  _bookmark() {
    this.setState({bookmark:!this.state.bookmark});
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{flex:1, width:'100%', marginBottom:60, backgroundColor:'white'}}>
          <Image style={{height:250, width:'100%', resizeMode:'stretch'}} source={require("../../assets/img/indinews-detail/banner.png")}/>
          <Text style={{fontSize:20, fontWeight:'bold', color:'black', margin:20}}>Kosan Al contrario di quanto si pensi parte hannno</Text>
          <ScrollView horizontal={true} style={{paddingLeft:10}}>
            <View style={{backgroundColor:'#e8e2ff', borderRadius:20, padding:10, paddingTop:5, paddingBottom:5, marginLeft:10}}>
              <Text style={{color:'purple', fontSize:12}}>Celebrity Style</Text>
            </View>
            <View style={{backgroundColor:'#e8e2ff', borderRadius:20, padding:10, paddingTop:5, paddingBottom:5, marginLeft:10}}>
              <Text style={{color:'purple', fontSize:12}}>Designers</Text>
            </View>
            <View style={{backgroundColor:'#e8e2ff', borderRadius:20, padding:10, paddingTop:5, paddingBottom:5, marginLeft:10}}>
              <Text style={{color:'purple', fontSize:12}}>Fashion</Text>
            </View>
            <View style={{backgroundColor:'#e8e2ff', borderRadius:20, padding:10, paddingTop:5, paddingBottom:5, marginLeft:10}}>
              <Text style={{color:'purple', fontSize:12}}>Shopping</Text>
            </View>
            <View style={{backgroundColor:'#e8e2ff', borderRadius:20, padding:10, paddingTop:5, paddingBottom:5, marginLeft:10, marginRight:20}}>
              <Text style={{color:'purple', fontSize:12}}>Trends</Text>
            </View>
          </ScrollView>
          <View style={{margin:20, flexDirection:'row', alignItems:'center'}}>
            <AntDesign name={"clockcircleo"} size={18} color={"lightgray"} />
            <Text style={{color:'lightgray', marginLeft:5}}>29 April 2019</Text>
          </View>
          <Text style={{marginLeft:20, marginRight:20}}>Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno subito delle variaziono del tempo, a causa dell’inserimento di passaggi ironici, o di sequenze casuali di caratteri palesemente poco verosimili. Se si decide di utilizzare un passaggio del Lorem Ipsum, e bene essere certi she non contenga nulla di imbarazzante. In genere, i generatori di testo segnaposto disponibili su internet tendono a ripetere paragrafi predefiniti, rendendo questo il primo vero generatore automatico su internet. Infatti utilizza un dizionario di oltre 200 vocaboli latini, combinati con un insieme di modelli di strutture di periodi, per generare passaggi di testo verosimili. Il testo cosi generato e sempre privo di ripetizioni, parole imbarazzanti o fuori iuogo ecc. rendendo questo il primo vero generatore automatico su internet. Infatti utilizza un dizionario di oltre 200 vocaboli latini, combinati</Text>
          <View style={{margin:20, flexDirection:'row'}}>
            <View style={{width:'10%', alignItems:'flex-end'}}>
              <Image style={{width:20, height:20, resizeMode:'contain'}} source={require("../../assets/img/indinews-detail/left-quote.png")}/>
            </View>
            <View style={{width:'80%', paddingLeft:5, paddingRight:5}}>
              <Text style={{fontStyle:'italic'}}>E universalmente riconosciuto che un lettore che osserva il layout di una pagina viene distratto dal contenuto testuale se questo e legg ibile. Lo scopo dell’utilizzo del Lorem Ipsum e che offre una nomale distribuzione delle lettere (al contrario di quanto avviene se si utilizzano brevi frasi ripetute, ad esempio “testo qui”), apparendo come un nomale blocco di testo leggibile. Molti software di impaginazione e di web design</Text>
            </View>
            <View style={{width:'10%', justifyContent:'flex-end'}}>
              <Image style={{width:20, height:20, resizeMode:'contain'}} source={require("../../assets/img/indinews-detail/right-quote.png")}/>
            </View>
          </View>
          <Text style={{marginLeft:20, marginRight:20, fontWeight:'bold', fontSize:16, color:'black'}}>Cose Lorem Ipsum?</Text>
          <Text style={{margin:20, marginTop:10}}>Lorem Ipsum e un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum e considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblo pre preparare un testo compione. E sopravvissuto non solo a piu di cinque secoli, ma anche al passaggio alla videoimpaginazione, prevenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenevano passaggi del Lorem Ipsum, e piu recentemente da software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.</Text>
          <Text style={{marginLeft:20, marginRight:20, color:'lightgray'}}>Gallery</Text>
          <View style={{padding:20}}>
            <Image style={{width:'100%', height:200, borderRadius:15}} source={require("../../assets/img/indinews-detail/gallery.png")}/>
          </View>
          <Text style={{margin:20, marginTop:0, fontWeight:'bold', fontSize:16, color:'black'}}>Da dove viene?</Text>
          <Text style={{margin:20, marginTop:0}}>Al contario di quanto si pensi, Lorem Ipsum non e semplicemente una sequenza casuale di caratteri. Risale ad un classico della letteratura latina del 45 AC, cosa che lo rende vecchio di 2000 anni. Richard McClintock, professore di latino al Hampden-Sydney Collage in Virginia, ha ricercato una delle piu oscure parole latine, consectetur.</Text>
          <View style={{margin:20, padding:10, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <TouchableOpacity style={{flexDirection:'row', alignItems:'center'}}>
              <FontAwesome name={"commenting-o"} size={25} color={"gray"}/>
              <Text style={{fontWeight:'500', marginLeft:10}}>View all 32 comments</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Entypo name={"share"} size={25} color={"gray"}/>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username,password) => dispatch(login(username,password)),
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(IndinewsDetail);
