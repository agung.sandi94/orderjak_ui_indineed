import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Body, Right, Title, Button, Icon, Card, CardItem, Text, Picker } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';

class Checkout extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this._setHidePopUp = this._setHidePopUp.bind(this);
    this.changeTotal = this.changeTotal.bind(this);
    this.state = {
      onClose: this._setHidePopUp,
      isEmpty: true,
      notes: "",
      total: 1,
      select:"1"
    };
  }

  _setHidePopUp() {
    this.setState({ isEmpty: false });
  }

  changeTotal(act){
    var total = this.state.total
    if (act == "minus"){
      if (total > 1){
        total = total - 1
        this.setState({total})
      }
    } else {
      total = total + 1
      this.setState({total})
    }
  }

  onSelectChange(value: string) {
    this.setState({
      select: value
    });
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'white'}}>
          <Left>
            <Button transparent onPress={()=>{this.props.navigation.goBack()}}>
              <Icon name='arrow-back' style={{color:'black'}} />
            </Button>
          </Left>
          <Body>
            <Title style={{color:'black', fontWeight:'bold'}}>Payment Method</Title>
          </Body>
          <Right>
          </Right>
        </Header>
        <Content style={{padding:20}}>
          <TouchableOpacity>
            <Card style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon  type="FontAwesome" name='ticket' style={{color:'#65c5f2', fontSize:20}} />
                <Text style={{fontSize:12, color:'#65c5f2', fontWeight:'600', marginLeft:10}}>Gunakan Kode Promo</Text>
              </View>
              <Icon  type="Ionicons" name='ios-arrow-forward' style={{color:'#65c5f2', fontSize:20}} />
            </Card>
          </TouchableOpacity>
          <Text style={{fontSize:12, marginTop:10, fontWeight:'600'}}>Alamat Pengiriman</Text>
          <Card style={{marginTop:10, padding:10}}>
            <View style={{width:'100%', flexDirection:'row'}}>
              <Text style={{fontSize:12, fontWeight:'600'}}>Agung Sandi</Text>
              <Text style={{fontSize:12, marginLeft:5, color:'gray', width:'65%'}} numberOfLines={1}>(Istana Paster Regency. Terusan Gn. Batu No.51, Sukaraja, Cicendo, Kota Bandung, Jawa Barat 40175, Cicendo, Kota Bandung 40175, 085221711234)</Text>
            </View>
            <View style={{width:'100%', borderColor:'lightgray', borderWidth:0.5, marginTop:10}} />
            <Text style={{fontSize:12, marginLeft:5, color:'gray', marginTop:10, lineHeight:18}}>(Istana Paster Regency. Terusan Gn. Batu No.51, Sukaraja, Cicendo, Kota Bandung, Jawa Barat 40175, Cicendo, Kota Bandung 40175, 085221711234)</Text>
            <Button style={{backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', width:'100%', height:35, marginTop:20}} onPress={()=>{this.props.navigation.navigate("PilihAlamatLain")}}>
              <Text style={{fontSize:12, fontWeight:'bold'}}>Pilih Alamat</Text>
            </Button>
          </Card>
          <Text style={{fontSize:12, marginTop:10, fontWeight:'600'}}>Daftar Pesanan</Text>
          <Card style={{marginTop:10, padding:10}}>
            <View style={{flexDirection:'row'}}>
              <Card style={{borderRadius:10}}>
                <Image style={{height:110, width:110, resizeMode:'stretch', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
              </Card>
              <View style={{padding:10}}>
                <Text style={{fontSize:13, width:175}} numberOfLines={1}>Reuben Sandwich</Text>
                <Text style={{fontSize:16, color:'#65c5f2', fontWeight:'bold', marginTop:5}}>RM 500</Text>
                <View style={{width:150, flexDirection:'row', borderRadius:15, borderColor:'lightgray', borderWidth:0.5, alignItems:'center', justifyContent:'center', marginTop:15, alignSelf:'center'}}>
                  <TouchableOpacity onPress={()=>{this.changeTotal('minus')}} style={{width:25, height:25, backgroundColor:'#ededed', alignItems:'center', justifyContent:'center', borderTopLeftRadius:15, borderBottomLeftRadius:15}}>
                    <Icon type="Feather" name='minus' style={{color:'gray', fontSize:12}}/>
                  </TouchableOpacity>
                  <Text style={{width:100, textAlign:'center', fontSize:12}}>{this.state.total}</Text>
                  <TouchableOpacity onPress={()=>{this.changeTotal('add')}} style={{width:25, height:25, backgroundColor:'#ededed', alignItems:'center', justifyContent:'center', borderTopRightRadius:15, borderBottomRightRadius:15}}>
                    <Icon type="Feather" name='plus' style={{color:'gray', fontSize:12}}/>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <Text style={{marginTop:20, fontSize:14}}>Tambah catatan untuk Pemilik Restoran</Text>
            <Item fixedLabel style={{marginBottom:20}}>
              <Input value={this.state.notes} onChangeText={(notes)=>this.setState({notes})} placeholder="Tulis catatan" style={{fontSize:14}}/>
            </Item>
          </Card>
          <Card style={{marginTop:-7, padding:10}}>
            <Text style={{fontSize:12, fontWeight:'400'}}>Payment Method</Text>
            <Card style={{marginTop:10, flexDirection:'row', height:50}}>
              <View style={{width:'100%', flexDirection:'row', alignItems:'center'}}>
                <Image style={{height:25, width:25, resizeMode:'stretch', left:2, position:'absolute'}} source={require('../../assets/img/credit-card.png')} />
                <Picker
                  mode="dropdown"
                  style={{width:undefined, height:'100%', marginLeft:85}}
                  textStyle={{ color: (this.state.select=="")?"gray":"black", fontSize:12 }}
                  itemTextStyle={{ color: (this.state.select=="")?"gray":"black", fontSize:12 }}
                  selectedValue={this.state.select}
                  onValueChange={this.onSelectChange.bind(this)}
                >
                  <Picker.Item label="CASH" value="1" />
                  <Picker.Item label="E-WALLET" value="2" />
                </Picker>
              </View>
            </Card>
            <Item fixedLabel style={{paddingTop:10, paddingBottom:10, borderBottomWidth:0.5, borderBottomColor:'lightgray', marginTop:20}}>
              <Label style={{fontSize:12}}>Harga Pesanan</Label>
              <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 500</Label>
            </Item>
            <Item fixedLabel style={{paddingTop:10, paddingBottom:10, borderBottomWidth:0.5, borderBottomColor:'lightgray'}}>
              <Label style={{fontSize:12}}>Jumlah Pesanan</Label>
              <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>1</Label>
            </Item>
            <Item fixedLabel style={{paddingTop:10, paddingBottom:10, borderBottomWidth:0.5, borderBottomColor:'lightgray'}}>
              <Label style={{fontSize:12}}>Biaya Kirim</Label>
              <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 10</Label>
            </Item>
            <Item fixedLabel style={{paddingTop:10, paddingBottom:10, borderBottomWidth:0, borderBottomColor:'lightgray', marginBottom:20}}>
              <Label style={{fontSize:12}}>Subtotal</Label>
              <Label style={{fontSize:12, textAlign:'right', fontWeight:'bold', color:'black'}}>RM 510</Label>
            </Item>
          </Card>
          <Text style={{fontSize:12, marginTop:10, fontWeight:'600'}}>Total Ringkasan Pesanan</Text>
          <Card style={{marginTop:10, padding:10, marginBottom:40}}>
            <Item fixedLabel style={{paddingTop:10, paddingBottom:10, borderBottomWidth:0.5, borderBottomColor:'lightgray'}}>
              <Label style={{fontSize:12}}>Harga Pesanan</Label>
              <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 500</Label>
            </Item>
            <Item fixedLabel style={{paddingTop:10, paddingBottom:10, borderBottomWidth:0.5, borderBottomColor:'lightgray'}}>
              <Label style={{fontSize:12}}>Biaya Kirim</Label>
              <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 10</Label>
            </Item>
            <Item fixedLabel style={{paddingTop:10, paddingBottom:10, borderBottomWidth:0, borderBottomColor:'lightgray'}}>
              <Label style={{fontSize:12, fontWeight:'bold', color:'black'}}>Total Belanja</Label>
              <Label style={{fontSize:12, textAlign:'right', fontWeight:'bold', color:'black'}}>RM 510</Label>
            </Item>
          </Card>
        </Content>
        <Card style={{marginBottom:0}}>
          <CardItem style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View>
              <Text style={{fontSize:12, color:'gray'}}>Total Tagihan</Text>
              <Text style={{color:'#65c5f2', fontWeight:'bold'}}>RM 510</Text>
            </View>
            <Button style={{backgroundColor:'#65c5f2', width:125, alignItems:'center', justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("Checkout")}>
              <Text style={{fontSize:12, color:'white', fontWeight:'bold'}}>Buy Now</Text>
            </Button>
          </CardItem>
        </Card>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Checkout);
