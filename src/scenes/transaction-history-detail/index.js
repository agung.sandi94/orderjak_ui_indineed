import React, {Component} from 'react';
import { Image, View } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, Text, Body } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';

class TransactionHistoryDetail  extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff', padding:20}}>
          <View style={{marginTop:0, width:'100%', alignItems:'center'}}>
            <Text style={{marginTop:10, fontSize:18, fontWeight:'bold'}}>Transaction History</Text>
            <Text style={{marginTop:5, fontSize:12, color:'gray'}}>Requested on :
              <Text style={{marginTop:5, fontSize:12, color:'black'}}> Rabu, 1 Januari 1970</Text>
            </Text>
          </View>
          <Button transparent style={{ position:'absolute', top:0, left: -20}} onPress={()=>this.props.navigation.goBack()}>
            <Icon type="Feather" name='arrow-left' style={{color:'black', fontSize:25}} />
          </Button>
          <Card style={{ marginTop:70, paddingTop:70, alignItems:'center', paddingBottom:10}}>
            <Card style={{position:'absolute', top:-35, height:70, width:70, borderRadius:15, alignSelf:'center', backgroundColor:'blue', alignItems:'center', justifyContent:'center'}}>
              <Image style={{height:60, width:60, resizeMode:'contain'}} source={require('../../assets/img/logo.png')} />
            </Card>
            <Text style={{fontSize:14, fontWeight:'600', textAlign:'center', width:'60%'}} numberOfLines={1}>Canon PIXMA TS207 Inkjet Canon PIXMA TS207 Inkjet</Text>
            <Text style={{fontSize:12, fontWeight:'600', textAlign:'center', marginTop:5, color:'gray'}}>RKR201901232</Text>
            <Card style={{width:'70%', height:110, borderRadius:50, marginTop:20, borderWidth:2, borderColor:'lightgray', padding:5}}>
              <Image style={{height:'100%', width:'100%', borderRadius:50, resizeMode:'stretch'}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
            </Card>
            <Text style={{marginTop:30, fontSize:14, fontWeight:'bold', color:'#65c5f2', alignSelf:'flex-start', marginLeft:10}}>Details</Text>
            <View style={{borderColor:'lightgray', borderWidth:0.5, width:'95%', margin:10}}/>
            <View style={{flexDirection:'row', justifyContent:'space-between', paddingLeft:10, paddingRight:10, width:'100%', marginBottom:10}}>
              <Text style={{fontSize:12, color:'gray'}}>Durasi</Text>
              <Text style={{fontSize:12, fontWeight:'600', color:'gray'}}>1/Bulan</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between', paddingLeft:10, paddingRight:10, width:'100%', marginBottom:10}}>
              <Text style={{fontSize:12, color:'gray'}}>Start Date</Text>
              <Text style={{fontSize:12, fontWeight:'600', color:'gray'}}>Kamis, 2 Januari 1970</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between', paddingLeft:10, paddingRight:10, width:'100%', marginBottom:10}}>
              <Text style={{fontSize:12, color:'gray'}}>Konfirmasi Admin</Text>
              <Text style={{fontSize:12, fontWeight:'600', color:'#65c5f2'}}>Tertunda</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between', paddingLeft:10, paddingRight:10, width:'100%', marginBottom:10}}>
              <Text style={{fontSize:12, color:'gray'}}>Status Pembayaran</Text>
              <Text style={{fontSize:12, fontWeight:'600', color:'#65c5f2'}}>Belum Dibayar</Text>
            </View>
          </Card>
          <Card style={{ marginTop:10, alignItems:'center', paddingBottom:10, marginBottom:30}}>
            <Text style={{marginTop:15, fontSize:14, fontWeight:'bold', color:'#65c5f2', alignSelf:'flex-start', marginLeft:10}}>Alamat Tagihan</Text>
            <View style={{borderColor:'lightgray', borderWidth:0.5, width:'95%', margin:10}}/>
            <View style={{flexDirection:'row', justifyContent:'space-between', paddingLeft:10, paddingRight:10, width:'100%', marginBottom:10}}>
              <Text style={{fontSize:12, color:'gray'}}>Nama</Text>
              <Text style={{fontSize:12, fontWeight:'600', color:'gray'}}>Agung sadega</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between', paddingLeft:10, paddingRight:10, width:'100%', marginBottom:10}}>
              <Text style={{fontSize:12, color:'gray'}}>Email</Text>
              <Text style={{fontSize:12, fontWeight:'600', color:'gray'}}>agung.sadega@gmail.com</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between', paddingLeft:10, paddingRight:10, width:'100%', marginBottom:10}}>
              <Text style={{fontSize:12, color:'gray'}}>Mobile Phone</Text>
              <Text style={{fontSize:12, fontWeight:'600', color:'gray'}}>08522171234</Text>
            </View>
          </Card>
        </Content>
        <Card style={{width:'100%', flexDirection:'row', justifyContent:'space-between', padding:10, marginBottom:0}}>
          <Button light style={{marginLeft:10, width:'45%', alignItems:'center', justifyContent:'center'}}>
            <Text style={{fontSize:12, fontWeight:'bold', color:'#65c5f2'}}> Exit </Text>
          </Button>
          <Button info style={{marginRight:10, width:'45%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2'}}>
            <Text style={{fontSize:12, fontWeight:'bold'}}> Approve </Text>
          </Button>
        </Card>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(TransactionHistoryDetail);
