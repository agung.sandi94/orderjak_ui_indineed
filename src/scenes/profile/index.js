import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Body, Right, Title, Button, Icon, Card, CardItem, Text, Picker, Thumbnail } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';
import DatePicker from 'react-native-datepicker'

class FormBooking extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this._simpanKontak = this._simpanKontak.bind(this);
    this._batalUbahKontak = this._batalUbahKontak.bind(this);
    this._simpanLainnya = this._simpanLainnya.bind(this);
    this._batalUbahLainnya = this._batalUbahLainnya.bind(this);
    this.state = {
      noHandphone: "",
      noHandphoneEdit: "",
      alamatLengkap: "",
      alamatLengkapEdit: "",
      ubahKontak: false,
      jenisKelamin: "",
      jenisKelaminEdit: "",
      tempatLahir: "",
      tempatLahirEdit: "",
      tanggalLahir: "",
      tanggalLahirEdit: "",
      kotaAsal: "",
      kotaAsalEdit: "",
      ubahLainnya: false
    };
  }

  _batalUbahKontak(){
    this.setState({
      noHandphoneEdit: this.state.noHandphone,
      alamatLengkapEdit: this.state.alamatLengkap,
      ubahKontak: false
    })
  }

  _simpanKontak(){
    this.setState({
      noHandphone: this.state.noHandphoneEdit,
      alamatLengkap: this.state.alamatLengkapEdit,
      ubahKontak: false
    });
  }

  _batalUbahLainnya(){
    this.setState({
      jenisKelaminEdit: this.state.jenisKelamin,
      tempatLahirEdit: this.state.tempatLahir,
      tanggalLahirEdit: this.state.tanggalLahir,
      kotaAsalEdit: this.state.kotaAsal,
      ubahLainnya: false
    })
  }

  _simpanLainnya(){
    this.setState({
      jenisKelamin: this.state.jenisKelaminEdit,
      tempatLahir: this.state.tempatLahirEdit,
      tanggalLahir: this.state.tanggalLahirEdit,
      kotaAsal: this.state.kotaAsalEdit,
      ubahLainnya: false
    });
  }

  _jenisKelaminChange(value: string) {
    this.setState({
      jenisKelaminEdit: value
    });
  }

  render() {
    return (
      <Container>
        <Content style={{padding:20}}>
          <Text style={{fontSize:18, fontWeight:'bold', marginBottom:5}}>User Profile</Text>
          <Card style={{flexDirection:'row', padding:10, alignItems:'center'}}>
            <Thumbnail large source={require("../../assets/img/kosan-detail/kosan.png")} />
            <View style={{marginLeft:10, width:'70%'}}>
              <Text style={{fontSize:14, fontWeight:'500'}}>Agung Sandi Maryono</Text>
              <Text style={{fontSize:12, marginTop:5, color:'gray'}}>agung.sandi94a@gmail.com</Text>
            </View>
          </Card>
          <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:10}}>
            <Button style={{width:'49%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2'}}>
              <Text style={{fontSize:12, fontWeight:'bold', color:'white'}}>Ubah Password</Text>
            </Button>
            <Button style={{width:'49%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2'}} onPress={()=>this.props.navigation.navigate("TransactionHistory")}>
              <Text style={{fontSize:12, fontWeight:'bold', color:'white'}}>Transaksi History</Text>
            </Button>
          </View>
          <View style={{flexDirection:'row', alignItems:'flex-end', marginBottom:5, marginTop:10}}>
            <Text style={{fontSize:14, fontWeight:'bold'}}>Infomasi Kontak</Text>
            { (!this.state.ubahKontak) &&
              <TouchableOpacity onPress={()=>this.setState({ubahKontak:!this.state.ubahKontak})}>
                <Text style={{fontSize:12, fontWeight:'600', color:'#65c5f2'}}> ( ubah )</Text>
              </TouchableOpacity>
            }
            { (this.state.ubahKontak) &&
              <TouchableOpacity onPress={this._batalUbahKontak}>
                <Text style={{fontSize:12, fontWeight:'600', color:'red'}}> ( batal )</Text>
              </TouchableOpacity>
            }
          </View>
          <Card style={{flexDirection:'row', paddingTop:5, paddingBottom:10, alignItems:'center'}}>
            <Form style={{width:'97%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, fontWeight:'bold'}}>No Handphone</Label>
                { (!this.state.ubahKontak) &&
                  <Input style={{fontSize:12, height:30, paddingLeft:5, paddingRight:5}} value={(this.state.noHandphone=="")?"-":this.state.noHandphone} editable={false}/>
                }
                { (this.state.ubahKontak) &&
                  <Input style={{fontSize:12, height:45, width:'100%', paddingLeft:10, paddingRight:10, marginBottom:5, marginTop:10, borderWidth:1, borderColor:'lightgray'}} value={this.state.noHandphoneEdit} onChangeText={(noHandphoneEdit) => this.setState({noHandphoneEdit})}/>
                }
              </Item>
              <Item stackedLabel>
                <Label style={{fontSize:12, fontWeight:'bold'}}>Alamat Lengkap</Label>
                { (!this.state.ubahKontak) &&
                  <Input style={{fontSize:12, height:30, paddingLeft:5, paddingRight:5}} value={(this.state.alamatLengkap=="")?"-":this.state.alamatLengkap} editable={false}/>
                }
                { (this.state.ubahKontak) &&
                  <Input multiline={true} style={{fontSize:12, height:75, width:'100%', paddingLeft:10, paddingRight:10, marginBottom:5, marginTop:10, borderWidth:1, borderColor:'lightgray'}} value={this.state.alamatLengkapEdit} onChangeText={(alamatLengkapEdit) => this.setState({alamatLengkapEdit})}/>
                }
              </Item>
              { (this.state.ubahKontak) &&
                <Item style={{marginTop:10}}>
                  <Button style={{width:'100%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2'}} onPress={this._simpanKontak}>
                    <Text style={{fontSize:12, fontWeight:'bold', color:'white'}}>Simpan</Text>
                  </Button>
                </Item>
              }
            </Form>
          </Card>
          <View style={{flexDirection:'row', alignItems:'flex-end', marginBottom:5, marginTop:10}}>
            <Text style={{fontSize:14, fontWeight:'bold'}}>Infomasi Lainnya</Text>
            { (!this.state.ubahLainnya) &&
              <TouchableOpacity onPress={()=>this.setState({ubahLainnya:!this.state.ubahLainnya})}>
                <Text style={{fontSize:12, fontWeight:'600', color:'#65c5f2'}}> ( ubah )</Text>
              </TouchableOpacity>
            }
            { (this.state.ubahLainnya) &&
              <TouchableOpacity onPress={this._batalUbahLainnya}>
                <Text style={{fontSize:12, fontWeight:'600', color:'red'}}> ( batal )</Text>
              </TouchableOpacity>
            }
          </View>
          <Card style={{flexDirection:'row', paddingTop:5, paddingBottom:10, alignItems:'center'}}>
            <Form style={{width:'97%'}}>
              <Item stackedLabel>
                <Label style={{fontSize:12, fontWeight:'bold'}}>Jenis Kelamin</Label>
                { (!this.state.ubahLainnya) &&
                  <Input style={{fontSize:12, height:30, paddingLeft:5, paddingRight:5}} value={(this.state.jenisKelamin=="")?"-":this.state.jenisKelamin} editable={false}/>
                }
                { (this.state.ubahLainnya) &&
                  <View style={{width:'100%', borderColor:'lightgray', borderWidth:1, marginTop:10, marginBottom:5}}>
                    <Picker
                      mode="dropdown"
                      style={{ width:'100%', paddingTop:10 }}
                      textStyle={{fontSize:12}}
                      itemTextStyle={{fontSize:12}}
                      selectedValue={this.state.jenisKelaminEdit}
                      onValueChange={this._jenisKelaminChange.bind(this)}
                    >
                      <Picker.Item label="Pilih jenis kelamin" value="" />
                      <Picker.Item label="Pria" value="Pria" />
                      <Picker.Item label="Wanita" value="Wanita" />
                    </Picker>
                  </View>
                }
              </Item>
              <Item stackedLabel>
                <Label style={{fontSize:12, fontWeight:'bold'}}>Tempat Lahir</Label>
                { (!this.state.ubahLainnya) &&
                  <Input style={{fontSize:12, height:30, paddingLeft:5, paddingRight:5}} value={(this.state.tempatLahir=="")?"-":this.state.tempatLahir} editable={false}/>
                }
                { (this.state.ubahLainnya) &&
                  <Input style={{fontSize:12, height:45, width:'100%', paddingLeft:10, paddingRight:10, marginBottom:5, marginTop:10, borderWidth:1, borderColor:'lightgray'}} value={this.state.tempatLahirEdit} onChangeText={(tempatLahirEdit) => this.setState({tempatLahirEdit})}/>
                }
              </Item>
              <Item stackedLabel>
                <Label style={{fontSize:12, fontWeight:'bold'}}>Tanggal Lahir</Label>
                { (!this.state.ubahLainnya) &&
                  <Input style={{fontSize:12, height:30, paddingLeft:5, paddingRight:5}} value={(this.state.tanggalLahir=="")?"-":this.state.tanggalLahir} editable={false}/>
                }
                { (this.state.ubahLainnya) &&
                  <DatePicker
                    style={{width: '100%', marginTop:10, marginBottom:5}}
                    date={this.state.tanggalLahirEdit}
                    mode="date"
                    placeholder="Tanggal Lahir"
                    format="DD-MM-YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    onDateChange={(date) => {this.setState({tanggalLahirEdit: date})}}
                  />
                }
              </Item>
              <Item stackedLabel>
                <Label style={{fontSize:12, fontWeight:'bold'}}>Kota Asal</Label>
                { (!this.state.ubahLainnya) &&
                  <Input style={{fontSize:12, height:30, paddingLeft:5, paddingRight:5}} value={(this.state.kotaAsal=="")?"-":this.state.kotaAsal} editable={false}/>
                }
                { (this.state.ubahLainnya) &&
                  <Input style={{fontSize:12, height:45, width:'100%', paddingLeft:10, paddingRight:10, marginBottom:5, marginTop:10, borderWidth:1, borderColor:'lightgray'}} value={this.state.kotaAsalEdit} onChangeText={(kotaAsalEdit) => this.setState({kotaAsalEdit})}/>
                }
              </Item>
              { (this.state.ubahLainnya) &&
                <Item style={{marginTop:10}}>
                  <Button style={{width:'100%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2'}} onPress={this._simpanLainnya}>
                    <Text style={{fontSize:12, fontWeight:'bold', color:'white'}}>Simpan</Text>
                  </Button>
                </Item>
              }
            </Form>
          </Card>
          <Button style={{width:'100%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2', marginTop:10, marginBottom:30}} onPress={()=>this.props.navigation.navigate("Login")}>
            <Text style={{fontSize:12, fontWeight:'bold', color:'white'}}>Logout</Text>
          </Button>
        </Content>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(FormBooking);
