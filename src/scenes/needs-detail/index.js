import React, {Component} from 'react';
import {Platform, StyleSheet, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';
import ShareTab from '../../components/share-tab';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import Octicons from 'react-native-vector-icons/dist/Octicons';
import { Rating } from 'react-native-elements';
import SplashScreen from 'react-native-smart-splash-screen';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Card, CardItem, Thumbnail, Label } from 'native-base';
import Swiper from "react-native-web-swiper";
import ReadMore from "react-native-read-more-text";

class NeedsDetail extends Component {
  static navigationOptions = ({navigation}) => ({
    header: null ,
    headerBackTitle: 'Back',
  });

  constructor(props) {
    super(props);
    this._setHidePopUp = this._setHidePopUp.bind(this);
    this.state = {
      activeTab:1,
      shareTab: false,
      closeShare: this._setHidePopUp
    }
  }

  _setHidePopUp() {
    this.setState({ shareTab: false });
  }

  _renderTruncatedFooter = (handlePress) => {
    return (
      <TouchableOpacity onPress={handlePress} style={{ marginTop: 10, width:'100%', alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
        <Text style={{color: '#65c5f2', fontWeight:'bold', fontSize:14}}>
          Read more
        </Text>
        <Icon type="Ionicons" name='ios-arrow-down' style={{color:'#65c5f2', fontSize:20, marginLeft:10}} />
      </TouchableOpacity>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <TouchableOpacity onPress={handlePress} style={{ marginTop: 10, width:'100%', alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
        <Text style={{color: '#65c5f2', fontWeight:'bold', fontSize:14}}>
          Show less
        </Text>
        <Icon type="Ionicons" name='ios-arrow-up' style={{color:'#65c5f2', fontSize:20, marginLeft:10}} />
      </TouchableOpacity>
    );
  }

  _handleTextReady = () => {
    // ...
  }

  render() {
    return (
      <Container>
        <Header style={{alignItems:'center', justifyContent:'space-between', paddingLeft:0, paddingRight:0, backgroundColor:'white'}}>
          <Button transparent style={{marginLeft:-5}}>
            <Icon type="Ionicons" name='md-list' style={{color:'#65c5f2'}} />
          </Button>
          <Title style={{color:'black', fontWeight:'600'}}></Title>
          <Button transparent style={{marginRight:-5}}>
            <Icon type="FontAwesome" name='shopping-basket' style={{color:'#65c5f2'}}  />
            <View style={{position:'absolute', top:5, right:20, backgroundColor:'red', borderColor:'white', borderWidth:2, height:16, width:16, borderRadius:8}}/>
          </Button>
        </Header>
        <Content>
          <Card style={{width:'100%',height:250, marginTop:0, marginLeft:0}}>
            <Swiper prevButtonText={""} nextButtonText={""} dotsWrapperStyle={{width:'100%', justifyContent:'flex-start'}} dotStyle={{backgroundColor:"white"}} activeDotStyle={{backgroundColor:"white", width:25, height:8}}>
              <Image style={{height:'100%', width:'100%', resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
              <Image style={{height:'100%', width:'100%', resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
              <Image style={{height:'100%', width:'100%', resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
            </Swiper>
          </Card>
          <Card style={{position:'absolute', height:60, width:60, borderRadius:30, top:210, right:20, alignItems:'center', justifyContent:'center', zIndex:100}}>
            <TouchableOpacity>
              <Icon type="FontAwesome" name="heart-o" style={{fontSize:27, color:"gray"}}/>
            </TouchableOpacity>
          </Card>
          <Card transparent>
            <CardItem>
              <Body>
                <Text style={{marginTop:20, fontSize:22, color:'black', fontWeight:'600'}}>McDonalds Johor</Text>
                <View style={{flexDirection:'row', marginTop:10, alignItems:'center'}}>
                  <Rating
                    imageSize={14}
                    readonly
                    startingValue={4.5}
                  />
                  <Text style={{fontSize:12, fontWeight:'300', color:'gray', marginLeft:5}}>4.5 / 5 (12 Tinjauan)</Text>
                </View>
                <View style={{flexDirection:'row', marginTop:20, alignItems:'center', justifyContent:'space-between', width:'100%'}}>
                  <Text style={{fontSize:24, fontWeight:'500', color:'#65c5f2'}}>RM 600</Text>
                  <TouchableOpacity onPress={()=>this.setState({shareTab:true})} style={{borderColor:'#65c5f2', borderWidth:1, borderRadius:25, flexDirection:'row', alignItems:'center', justifyContent:'center', padding:8, paddingLeft:15, paddingRight:15}}>
                    <Icon type="Feather" name='share-2' style={{color:'#65c5f2', fontSize:18}} />
                    <Text style={{color:'#65c5f2', fontWeight:'600', marginLeft:5, fontSize:12}}>Share</Text>
                  </TouchableOpacity>
                </View>
                <View style={{width:"200%", borderColor:'lightgray', borderWidth:0.5, marginTop:30, marginLeft:-50, marginRight:-50}} />
                <Text style={{marginTop:20, fontWeight:'600', fontSize:16}}>List Menu</Text>
                <Item fixedLabel style={{paddingTop:20, paddingBottom:20}}>
                  <Label style={{fontSize:12}}>Hamburger</Label>
                  <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 500</Label>
                </Item>
                <Item fixedLabel style={{paddingTop:20, paddingBottom:20}}>
                  <Label style={{fontSize:12}}>Reuben Sandwich</Label>
                  <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 570</Label>
                </Item>
                <Item fixedLabel style={{paddingTop:20, paddingBottom:20}}>
                  <Label style={{fontSize:12}}>Club Sandwich</Label>
                  <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 490</Label>
                </Item>
                <Item fixedLabel style={{paddingTop:20, paddingBottom:20}}>
                  <Label style={{fontSize:12}}>Chicken Breast</Label>
                  <Label style={{fontSize:12, textAlign:'right', fontWeight:'500', color:'black'}}>RM 510</Label>
                </Item>
                <Text style={{marginTop:25, fontWeight:'600', fontSize:16}}>Description</Text>
                <View style={{ marginTop:25}}>
                  <ReadMore
                    numberOfLines={5}
                    renderTruncatedFooter={this._renderTruncatedFooter}
                    renderRevealedFooter={this._renderRevealedFooter}
                    onReady={this._handleTextReady}>
                    <Text style={{ fontSize:12, lineHeight:18, color:'gray'}}>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas?
                    </Text>
                  </ReadMore>
                </View>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem style={{backgroundColor:'#eeeeee'}}>
              <View style={{flexDirection:'row', padding:20}}>
                <Icon type="Entypo" name='bell' style={{color:'#65c5f2', fontSize:30, width:30}} />
                <View style={{marginLeft:10}}>
                  <Text style={{color:'gray', fontSize:12, marginLeft:5}}>Pesan Sebelum</Text>
                  <View style={{flexDirection:'row', marginTop:2, alignItems:'center'}}>
                    <Text style={{fontSize:16, color:'#65c5f2', fontWeight:'500'}}> 10:00 PM </Text>
                    <Text style={{fontSize:12, color:'black' }}> Agar Mendapatkan Makan Malam Gratis </Text>
                  </View>
                  <View style={{marginTop:10, flexDirection:'row', marginLeft:5, alignItems:'center'}}>
                    <Icon type="MaterialIcons" name='access-time' style={{color:'#65c5f2', fontSize:14, width:20}} />
                    <Text style={{fontSize:12, color:'#65c5f2'}}>Waktu : <Text style={{fontWeight:'600', fontSize:12, color:'#65c5f2'}} >1 Jam 29 Menit</Text></Text>
                  </View>
                </View>
              </View>
            </CardItem>
          </Card>
          <Card>
            <CardItem style={{marginTop:-10}}>
              <View style={{flexDirection:'row', width:'100%', padding:20, justifyContent:'center', alignItems:'center'}}>
                <Icon type="MaterialIcons" name='access-time' style={{color:'#65c5f2', fontSize:18, width:20}} />
                <Text style={{fontSize:12, color:'gray', marginLeft:5}}>Last Update : 26 November at 11 : 40</Text>
              </View>
            </CardItem>
            <View style={{width:'120%', borderColor:'lightgray', borderWidth:0.5}} />
            <CardItem>
              <Body>
                <Text style={{marginTop:20, fontWeight:'600', fontSize:16}}>Comments</Text>
                <View style={{width:'100%', flexDirection:'row', marginTop:30}}>
                  <Thumbnail square source={require("../../assets/img/indineeds-search/categories-indineeds/1user.jpeg")} />
                  <View style={{marginLeft:10}}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                      <Text style={{fontSize:12, color:'#65c5f2', fontWeight:'bold'}}>Jake Franklin</Text>
                      <Text style={{fontSize:9, color:'gray', fontWeight:'400', marginLeft:10}}>2 Days ago</Text>
                    </View>
                    <Text style={{marginTop:10, fontSize:12, width:'36%', color:'gray'}}>Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno subito delie variazioni del tempo,</Text>
                    <View style={{flexDirection:'row', alignItems:'center', marginTop:10}}>
                      <Text style={{fontSize:12, color:'gray'}}>12</Text>
                      <Icon type="Ionicons" name='ios-arrow-up' style={{color:'gray', fontSize:12, width:10, marginLeft:10}} />
                      <Text style={{fontSize:12, color:'gray', marginLeft:10}}>|</Text>
                      <Icon type="Ionicons" name='ios-arrow-down' style={{color:'gray', fontSize:12, width:10, marginLeft:10}} />
                      <TouchableOpacity style={{flexDirection:'row'}}>
                        <Icon type="Entypo" name='dot-single' style={{color:'gray', fontSize:14, width:10, marginLeft:10}} />
                        <Text style={{fontSize:12, color:'gray', marginLeft:10}}>Reply</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>this.setState({shareTab:true})} style={{flexDirection:'row'}}>
                        <Icon type="Entypo" name='dot-single' style={{color:'gray', fontSize:14, width:10, marginLeft:10}} />
                        <Text style={{fontSize:12, color:'gray', marginLeft:10}}>Share</Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{borderWidth:0.5, borderColor:'lightgray', marginTop:15, width:'36%'}} />
                    <View style={{marginTop:30, flexDirection:'row'}}>
                      <Thumbnail square source={require("../../assets/img/indineeds-search/categories-indineeds/user2.jpeg")} />
                      <View style={{marginLeft:10}}>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                          <Text style={{fontSize:12, color:'#65c5f2', fontWeight:'bold'}}>Josh Mans</Text>
                          <Text style={{fontSize:9, color:'gray', fontWeight:'400', marginLeft:10}}>2 Days ago</Text>
                        </View>
                        <Text style={{marginTop:10, fontSize:12, width:'32%', color:'gray'}}>Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno subito delie</Text>
                        <View style={{flexDirection:'row', alignItems:'center', marginTop:10}}>
                          <Text style={{fontSize:12, color:'gray'}}>12</Text>
                          <Icon type="Ionicons" name='ios-arrow-up' style={{color:'gray', fontSize:12, width:10, marginLeft:10}} />
                          <Text style={{fontSize:12, color:'gray', marginLeft:10}}>|</Text>
                          <Icon type="Ionicons" name='ios-arrow-down' style={{color:'gray', fontSize:12, width:10, marginLeft:10}} />
                          <TouchableOpacity style={{flexDirection:'row'}}>
                            <Icon type="Entypo" name='dot-single' style={{color:'gray', fontSize:14, width:10, marginLeft:10}} />
                            <Text style={{fontSize:12, color:'gray', marginLeft:10}}>Reply</Text>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={()=>this.setState({shareTab:true})} style={{flexDirection:'row'}}>
                            <Icon type="Entypo" name='dot-single' style={{color:'gray', fontSize:14, width:10, marginLeft:10}} />
                            <Text style={{fontSize:12, color:'gray', marginLeft:10}}>Share</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{borderWidth:0.5, borderColor:'lightgray', marginTop:15, width:'100%'}} />
                <TouchableOpacity style={{marginTop:20, marginBottom:20}} onPress={()=>{this.props.navigation.navigate("Comments")}}>
                  <Text style={{color:"#65c5f2", fontSize:12, fontWeight:'bold', textDecorationLine:'underline'}}>See All Talks (41)</Text>
                </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem style={{backgroundColor:'#eeeeee', marginTop:-10, paddingBottom:30}}>
              <Body>
                <Text style={{marginTop:20, fontWeight:'600', fontSize:16}}>Other Food and Drinks</Text>
                <ScrollView horizontal={true} style={{ width:'110%', marginTop:20, flexDirection:'row'}}>
                  <View style={{width:Dimensions.get('window').width*0.42, marginRight:15}}>
                    <Card>
                      <Image style={{height:Dimensions.get('window').width*0.42, width:'100%', resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
                    </Card>
                    <Text style={{marginTop:10, fontSize:12, color:'black'}}>Teiwe Swiss TW2001CS-W1 Leather Streap</Text>
                    <Text style={{marginTop:10, fontSize:12, color:'#65c5f2', fontWeight:'bold'}}>RM 100</Text>
                  </View>
                  <View style={{width:Dimensions.get('window').width*0.42, marginRight:15}}>
                    <Card>
                      <Image style={{height:Dimensions.get('window').width*0.42, width:'100%', resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/2.jpg")}/>
                    </Card>
                    <Text style={{marginTop:10, fontSize:12, color:'black'}}>Teiwe Swiss TW2001CS-W1 Leather Streap</Text>
                    <Text style={{marginTop:10, fontSize:12, color:'#65c5f2', fontWeight:'bold'}}>RM 210</Text>
                  </View>
                  <View style={{width:Dimensions.get('window').width*0.42, marginRight:30}}>
                    <Card>
                      <Image style={{height:Dimensions.get('window').width*0.42, width:'100%', resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/5.jpg")}/>
                    </Card>
                    <Text style={{marginTop:10, fontSize:12, color:'black'}}>Teiwe Swiss TW2001CS-W1 Leather Streap</Text>
                    <Text style={{marginTop:10, fontSize:12, color:'#65c5f2', fontWeight:'bold'}}>RM 120</Text>
                  </View>
                </ScrollView>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem style={{marginTop:-10}}>
              <Body style={{flexDirection:'row', justifyContent:'space-between'}}>
                <Button bordered style={{borderColor:'gray'}} onPress={()=>{this.props.navigation.navigate("Chat")}}>
                  <Icon type="Ionicons" name='ios-chatbubbles' style={{color:'gray'}} />
                </Button>
                <Button bordered style={{ width:'40%', alignItems:'center', justifyContent:'center', borderColor:'#65c5f2'}} onPress={()=>{this.props.navigation.navigate("IndineedsBasket")}}>
                  <Text style={{color:'#65c5f2', fontSize:12, fontWeight:'bold'}}>Add to Basket</Text>
                </Button>
                <Button style={{ width:'40%', alignItems:'center', justifyContent:'center', backgroundColor:'#65c5f2'}} onPress={()=>{this.props.navigation.navigate("Checkout")}}>
                  <Text style={{color:'white', fontSize:12, fontWeight:'bold'}}>Buy Now</Text>
                </Button>
              </Body>
            </CardItem>
          </Card>
        </Content>
        <ShareTab navigate={this.props.navigation.navigate} isVisible={this.state.shareTab} onClose={this.state.closeShare} />
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(NeedsDetail);
