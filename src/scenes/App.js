import React from 'react';
import { Root } from "native-base";
import AppNavigator from './AppNavigator';
import { connect } from "react-redux";

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      token:'',
      user:[]
    }
  }

  render() {
    return (
      <Root>
        <AppNavigator/>
      </Root>
    );
  }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps)(App);
