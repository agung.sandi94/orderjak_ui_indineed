import React, {Component} from 'react';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, CardItem, Text, Body, Picker } from 'native-base';
import { connect } from 'react-redux';

class TransactionHistoryIndineed extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff', padding:20}}>
          <Text style={{marginTop:20, fontSize:20, fontWeight:'bold'}}>Riwayat Transaksi</Text>
          <View style={{flexDirection:'row', width:'100%', justifyContent:'space-between', alignItems:'flex-start', padding:10, marginTop:10}}>
            <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
              <Card style={{ width:60, height:60, borderRadius:30, alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                <TouchableOpacity style={{width:60, height:60, borderRadius:30, backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center'}}>
                  <Icon type="FontAwesome5" name="shopping-basket" style={{color:'white', fontSize:20}} />
                </TouchableOpacity>
              </Card>
              <Text style={{marginTop:5, fontSize:12, textAlign:'center'}}>Semua</Text>
            </View>
            <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
              <Card style={{ width:60, height:60, borderRadius:30, alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                <TouchableOpacity style={{width:60, height:60, borderRadius:30, backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center'}}>
                  <Icon type="FontAwesome5" name="wallet" style={{color:'white', fontSize:20}} />
                </TouchableOpacity>
              </Card>
              <Text style={{marginTop:5, fontSize:12, textAlign:'center'}}>Bayar Pesanan</Text>
            </View>
            <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
              <Card style={{ width:60, height:60, borderRadius:30, alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                <TouchableOpacity style={{width:60, height:60, borderRadius:30, backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center'}}>
                  <Icon type="FontAwesome" name="refresh" style={{color:'white', fontSize:20}} />
                </TouchableOpacity>
              </Card>
              <Text style={{marginTop:5, fontSize:12, textAlign:'center'}}>Dalam Proses</Text>
            </View>
            <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
              <Card style={{ width:60, height:60, borderRadius:30, alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                <TouchableOpacity style={{width:60, height:60, borderRadius:30, backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center'}}>
                  <Icon type="FontAwesome5" name="truck" style={{color:'white', fontSize:20}} />
                </TouchableOpacity>
              </Card>
              <Text style={{marginTop:5, fontSize:12, textAlign:'center'}}>Dalam Pengiriman</Text>
            </View>
          </View>

        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(TransactionHistoryIndineed);
