import React, {Component} from 'react';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, CardItem, Text, Body, Picker, Title, Tab, Tabs, TabHeading, ScrollableTab, Right } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';
import Indikos from './indikos';
import Indineed from './indineed';

class TransactionHistory  extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      select:""
    };
  }

  onSelectChange(value: string) {
    this.setState({
      select: value
    });
  }

  render() {
    return (
      <Container>
        <Header hasTabs style={{backgroundColor:'white'}}>
          <Left>
            <Button transparent onPress={()=>this.props.navigation.goBack()}>
              <Icon name='arrow-back' style={{color:'black'}} />
            </Button>
          </Left>
          <Body>
            <Title style={{color:'black', fontWeight:'600'}}>Transaksi</Title>
          </Body>
          <Right>
          </Right>
        </Header>
        <Tabs renderTabBar={()=> <ScrollableTab style={{backgroundColor:'white'}} underlineStyle={{backgroundColor:'#65c5f2'}} />}>
          <Tab heading={ <TabHeading style={{backgroundColor:'white'}}><Text style={{color:'#65c5f2'}}>Indikos</Text></TabHeading> }>
            <Indikos navigation={this.props.navigation} />
          </Tab>
          <Tab heading={ <TabHeading style={{backgroundColor:'white'}}><Text style={{color:'#65c5f2'}}>Indineeds</Text></TabHeading> }>
            <Indineed />
          </Tab>
        </Tabs>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(TransactionHistory);
