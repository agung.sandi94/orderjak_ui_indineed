import React, {Component} from 'react';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, CardItem, Text, Body, Picker } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';

class TransactionHistory  extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      select:"1"
    };
  }

  onSelectChange(value: string) {
    this.setState({
      select: value
    });
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff', padding:20}}>
          <Text style={{marginTop:20, fontSize:20, fontWeight:'bold'}}>Riwayat Transaksi</Text>
          <View picker style={{width:'100%', fontSize:12, marginTop:20, height:40}}>
            <Picker
              mode="dropdown"
              style={{width:'100%', borderColor:'lightgray', borderWidth:0.5, backgroundColor:'#f7f9fc'}}
              textStyle={{ color: (this.state.select=="")?"gray":"black", fontSize:12 }}
              selectedValue={this.state.select}
              onValueChange={this.onSelectChange.bind(this)}
            >
              <Picker.Item label="Transaction Booking" value="1" />
              <Picker.Item label="Permintaan Survey" value="2" />
            </Picker>
            <Icon type="Ionicons" name='ios-arrow-down' style={{color:'lightgray', fontSize:20, position:'absolute', top: 12, right:20}} />
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{marginTop:40}}>
            <TouchableOpacity style={{padding:5, paddingLeft:10, paddingRight:10, backgroundColor:'#ededed', borderRadius:5, margin:5}}>
              <Text style={{fontSize:14, fontWeight:'600'}}>All</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{padding:5, paddingLeft:10, paddingRight:10, borderRadius:5, margin:5}}>
              <Text style={{fontSize:14}}>Pending</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{padding:5, paddingLeft:10, paddingRight:10, borderRadius:5, margin:5}}>
              <Text style={{fontSize:14}}>Confirmed</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{padding:5, paddingLeft:10, paddingRight:10, borderRadius:5, margin:5}}>
              <Text style={{fontSize:14}}>Finished</Text>
            </TouchableOpacity>
          </ScrollView>
          <Card style={{marginTop:20}}>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', height:40}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon type="AntDesign" name="copy1" style={{marginLeft:10, fontSize:16, color:'gray'}} />
                <Text style={{fontSize:12, fontWeight:'600', color:'gray', marginLeft:5}}>RKR201901232</Text>
              </View>
              <Button disabled light style={{borderRadius:0, paddingLeft:5, paddingRight:5, height:40}}>
                <Text style={{color:'#65c5f2', fontSize:14, fontWeight:'bold'}}> Tertunda </Text>
              </Button>
            </View>
            <View style={{borderColor:'lightgray', borderWidth:0.5}}></View>
            <View style={{flexDirection:'row', padding:5}}>
              <Card style={{borderRadius:20}}>
                <Image style={{height:90, width:90, resizeMode:'stretch', borderRadius:20}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              </Card>
              <View style={{marginLeft:10, marginTop:10}}>
                <Text style={{fontSize:14, fontWeight:'600', width:200}} numberOfLines={1}>Kosan Al contrario</Text>
                <Text style={{fontSize:12, color:'gray', marginTop:10}}>Durasi :
                  <Text style={{fontSize:12, fontWeight:'600'}}> 1/Bulan</Text>
                </Text>
                <Text style={{fontSize:12, color:'gray', marginTop:5}}>Start Date :
                  <Text style={{fontSize:12, fontWeight:'600'}}> Kamis, 1 Januari 1970</Text>
                </Text>
                <Text style={{fontSize:12, color:'gray', marginTop:5}}>Status Pembayaran :
                  <Text style={{fontSize:12, fontWeight:'600'}}> Belum Dibayar</Text>
                </Text>
              </View>
            </View>
            <View style={{borderColor:'lightgray', borderWidth:0.5}}></View>
            <View style={{flexDirection:'row', justifyContent:'space-between', padding:10}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon type="FontAwesome" name="home" style={{width:18, fontSize:18, color:'#65c5f2'}} />
                <Text style={{fontSize:14, fontWeight:'600', color:'#65c5f2', marginLeft:5}}>Booking</Text>
              </View>
              <Button style={{backgroundColor:'#65c5f2', height:40, width:'65%', alignItems:'center', justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("TransactionHistoryDetail")}>
                <Text style={{fontSize:14, fontWeight:'bold', color:'white'}}> Lihat </Text>
              </Button>
            </View>
          </Card>
          <Card style={{marginTop:20, marginBottom:40}}>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', height:40}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon type="AntDesign" name="copy1" style={{marginLeft:10, fontSize:16, color:'gray'}} />
                <Text style={{fontSize:12, fontWeight:'600', color:'gray', marginLeft:5}}>RKR201901232</Text>
              </View>
              <Button disabled light style={{borderRadius:0, paddingLeft:5, paddingRight:5, height:40}}>
                <Text style={{color:'#65c5f2', fontSize:14, fontWeight:'bold'}}> Tertunda </Text>
              </Button>
            </View>
            <View style={{borderColor:'lightgray', borderWidth:0.5}}></View>
            <View style={{flexDirection:'row', padding:5}}>
              <Card style={{borderRadius:20}}>
                <Image style={{height:90, width:90, resizeMode:'stretch', borderRadius:20}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              </Card>
              <View style={{marginLeft:10, marginTop:10}}>
                <Text style={{fontSize:14, fontWeight:'600', width:200}} numberOfLines={1}>Canon PIXMA TS207 Inkjet Canon PIXMA TS207 Inkjet</Text>
                <Text style={{fontSize:12, color:'gray', marginTop:10}}>Durasi :
                  <Text style={{fontSize:12, fontWeight:'600'}}> 1/Bulan</Text>
                </Text>
                <Text style={{fontSize:12, color:'gray', marginTop:5}}>Start Date :
                  <Text style={{fontSize:12, fontWeight:'600'}}> Kamis, 1 Januari 1970</Text>
                </Text>
                <Text style={{fontSize:12, color:'gray', marginTop:5}}>Status Pembayaran :
                  <Text style={{fontSize:12, fontWeight:'600'}}> Belum Dibayar</Text>
                </Text>
              </View>
            </View>
            <View style={{borderColor:'lightgray', borderWidth:0.5}}></View>
            <View style={{flexDirection:'row', justifyContent:'space-between', padding:10}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon type="FontAwesome" name="shopping-cart" style={{width:18, fontSize:18, color:'#65c5f2'}} />
                <Text style={{fontSize:14, fontWeight:'600', color:'#65c5f2', marginLeft:5}}>Indineeds</Text>
              </View>
              <Button style={{backgroundColor:'#65c5f2', height:40, width:'65%', alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:14, fontWeight:'bold', color:'white'}}> Lihat </Text>
              </Button>
            </View>
          </Card>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(TransactionHistory);
