import {USER} from '../../services'

export const LOGIN_STATE = {
  FETCHING_DATA: 'FETCHING_LOGIN_DATA',
  FETCHING_DATA_SUCCESS: 'FETCHING_LOGIN_DATA_SUCCESS',
  FETCHING_DATA_FAILURE: 'FETCHING_LOGIN_DATA_FAILURE',
  CHECKING_DATA_FINISH: 'CHECKING_LOGIN_DATA_FINISH'
}

function getData() {
  return {
    type: LOGIN_STATE.FETCHING_DATA
  }
}

function getDataSuccess(data) {
  return {
    type: LOGIN_STATE.FETCHING_DATA_SUCCESS,
    data: data,
  }
}

function getDataFailure(message) {
  return {
    type: LOGIN_STATE.FETCHING_DATA_FAILURE,
    message: message
  }
}

export function finishCheck(){
  return {
    type: LOGIN_STATE.CHECKING_DATA_FINISH
  }
}

export function login(params) {
  return (dispatch) => {
    dispatch(getData())
    USER.login(params)
      .then((data) => {
        if (data.status == 200){
          result = data.data;
          if (result.status) {
            dispatch(getDataSuccess(result.data))
          } else {
            dispatch(getDataFailure(result.message))
          }
        } else {
            dispatch(getDataFailure("Something Wrong"))
        }
      })
      .catch((err) => {
        dispatch(getDataFailure(err.data.message))
      })
  }
}
