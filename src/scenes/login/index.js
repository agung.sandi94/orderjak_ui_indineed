import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, SectionList, Modal} from 'react-native';
import { connect } from 'react-redux';
import { login, finishCheck } from './actions'
import { styles } from "../../components/stylesheet";
import SplashScreen from 'react-native-smart-splash-screen';
import LoadingView from '../../components/loadingView';
import Loading from '../../components/loading';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { Spinner, Toast } from "native-base";

class Login extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.pressLogin = this.pressLogin.bind(this)
    this.handleFacebookLogin = this.handleFacebookLogin.bind(this)
    this.state = {
      username: "",
      password: "",
      remember:false,
      isLoading: true
    };
  }

  componentDidMount () {
    //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
    setTimeout(() => {
      this.setState({isLoading:false},()=>{
        SplashScreen.close({
          animationType: SplashScreen.animationType.scale,
          duration: 850,
          delay: 500,
        })
      });
    }, 5000);
  }

  componentWillReceiveProps(nextProps){
    console.log("NEXT PROPS", nextProps);
    if (nextProps.error){
      Toast.show({
        text: nextProps.message,
        textStyle: { color: "white", fontWeight:'600' },
        buttonText: "",
        type: "danger",
        duration: 3000
      });
      this.props.finishCheck()
    }

    if (nextProps.data){
      Toast.show({
        text: "Login Success",
        textStyle: { color: "white", fontWeight:'600' },
        buttonText: "",
        type: "success",
        duration: 3000
      });
      this.props.finishCheck()
      this.props.navigation.navigate("IndineedsSearch")
    }
  }

  handleFacebookLogin(){
    var nav = this.props.navigation;
    LoginManager.logInWithReadPermissions(["public_profile",'email']).then(
      function(result) {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          // FBGraphRequest('id, email, name', this.FBLoginCallback);
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              let accessToken = data.accessToken

              const responseInfoCallback = (error, result) => {
                if (error) {
                  Toast.show({
                    text: "Login Failed",
                    textStyle: { color: "white", fontWeight:'600' },
                    buttonText: "",
                    type: "danger",
                    duration: 3000
                  })
                } else {
                  Toast.show({
                    text: "Login Success",
                    textStyle: { color: "white", fontWeight:'600' },
                    buttonText: "",
                    type: "success",
                    duration: 3000
                  })
                  nav.navigate("IndineedsSearch")
                }
              }

              const infoRequest = new GraphRequest('/me', {
                  accessToken: accessToken,
                  parameters: {
                    fields: {
                      string: 'id, email, name'
                    }
                  }
                },responseInfoCallback
              );

              new GraphRequestManager().addRequest(infoRequest).start()
            }
          )
        }
      },
      function(error) {
        Toast.show({
          text: error,
          textStyle: { color: "white", fontWeight:'600' },
          buttonText: "",
          type: "danger",
          duration: 3000
        })
      }
    );
  }

  FBGraphRequest(fields, callback) {
    const accessData = AccessToken.getCurrentAccessToken();
    // Create a graph request asking for user information
    const infoRequest = new GraphRequest('/me', {
      accessToken: accessData.accessToken,
      parameters: {
        fields: {
          string: fields
        }
      }
    }, callback.bind(this));
    // Execute the graph request created above
    new GraphRequestManager().addRequest(infoRequest).start();
  }

  FBLoginCallback(error, result) {
    if (error) {
      console.log("ERROR");
    } else {
      console.log("SECCESS");
    }
  }

  successLoginFacebook(){
    this.props.navigation.navigate("IndineedsSearch")
  }

  pressLogin(){
    if (this.state.username == "") {
      return (
        Toast.show({
          text: "Please fill username!",
          textStyle: { color: "white", fontWeight:'600' },
          buttonText: "",
          type: "danger",
          duration: 3000
        })
      );
    }

    if (this.state.password == "") {
      return (
        Toast.show({
          text: "Please fill password!",
          textStyle: { color: "white", fontWeight:'600' },
          buttonText: "",
          type: "danger",
          duration: 3000
        })
      );
    }

    var params = {
      username: this.state.username,
      password: this.state.password
    }

    this.props.login(params)
  }

  render() {
    return (
      <View style={styles.container}>
        <LoadingView visible={this.state.isLoading} />
        <Image style={styles.logo} source={require('../../assets/img/logo.png')} />
        <Text style={styles.title}>Login</Text>
        <TouchableOpacity onPress={this.handleFacebookLogin} style={{width:'80%', height:50, backgroundColor:'#3b5998', flexDirection:'row', alignItems:'center', justifyContent:'center', borderRadius:25, marginTop:20}}>
          <Image style={{width:25, height:25, resizeMode:'contain'}} source={require('../../assets/img/icon-fb.png')} />
          <Text style={{color:'white', fontWeight:'bold', marginLeft:10}}>Continue with Facebook</Text>
        </TouchableOpacity>
        <Text style={styles.header}>Please login to your account</Text>
        <TextInput
          ref={(input) => { this.firstTextInput = input; }}
          style={styles.textInput}
          placeholder={"Username"}
          onChangeText={(username) => this.setState({username})}
          value={this.state.username}
          onSubmitEditing={() => { this.secondTextInput.focus(); }}
          blurOnSubmit={false}
          returnKeyType = { "next" }
        />
        <TextInput
          ref={(input) => { this.secondTextInput = input; }}
          style={styles.textInput}
          placeholder={"Password"}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
          secureTextEntry={true}
        />
        <View style={{marginTop:15, width:'80%', justifyContent:'flex-end', alignItems:'center', flexDirection:'row'}}>
          <TouchableOpacity onPress={()=>this.setState({remember:!this.state.remember})}>
            <MaterialCommunityIcons name={(this.state.remember)?"checkbox-marked-outline":"checkbox-blank-outline"} size={20} />
          </TouchableOpacity>
          <Text style={{marginLeft:5}}>Remember Me</Text>
        </View>
        <TouchableOpacity style={styles.btnBlue} onPress={this.pressLogin}>
          <Text style={styles.textBtnBlue}>Enter</Text>
        </TouchableOpacity>
        <View style={{marginTop:15, flexDirection:'row'}}>
          <Text style={[styles.header,{marginTop:0}]}>Don't have an account?</Text>
          <TouchableOpacity style={styles.hyperlink} onPress={()=>this.props.navigation.navigate('Register')}>
            <Text style={styles.hyperlinkText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
        <Loading visible={this.props.isFetching} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username,password) => dispatch(login(username,password)),
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Login);
