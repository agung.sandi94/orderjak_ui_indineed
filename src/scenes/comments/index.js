import React, {Component} from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, CardItem, Text, Body, Right, Picker, Thumbnail } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';

class ProsesPembayaran  extends Component {
  static navigationOptions = {
    title: "Comments",
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff'}}>
          <View style={{padding:20, flexDirection:'row'}}>
            <Thumbnail source={require("../../assets/img/indineeds-search/categories-indineeds/user2.jpeg")} />
            <View style={{marginLeft:10, width:'85%'}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:14, fontWeight:'600'}}>Avril Lavigne</Text>
                <Icon type="Entypo" name="dot-single" style={{fontSize: 12, color: 'gray', width:10, marginLeft:5}}/>
                <Text style={{marginLeft:5, fontSize:12, color:'gray'}}>5 min ago</Text>
              </View>
              <Text style={{color:'gray', fontSize:12, flex:1, lineHeight:16, marginTop:10, marginRight:20}}>Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno subito delie variazioni del tempo.</Text>
              <View style={{flexDirection:'row', marginTop:10, justifyContent:'space-between'}}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <TouchableOpacity>
                    <Text style={{fontSize:12, color:'gray'}}>Reply</Text>
                  </TouchableOpacity>
                  <Text style={{fontSize:12, color:'red', marginLeft:15, fontWeight:'600'}}>You Like</Text>
                </View>
                <TouchableOpacity style={{marginRight:20}}>
                  <Icon type="AntDesign" name="heart" style={{fontSize: 20, color: 'red'}}/>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{padding:20, flexDirection:'row'}}>
            <Thumbnail source={require("../../assets/img/indineeds-search/categories-indineeds/1user.jpeg")} />
            <View style={{marginLeft:10, width:'85%'}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:14, fontWeight:'600'}}>Kelly Clarkson</Text>
                <Icon type="Entypo" name="dot-single" style={{fontSize: 12, color: 'gray', width:10, marginLeft:5}}/>
                <Text style={{marginLeft:5, fontSize:12, color:'gray'}}>15 min ago</Text>
              </View>
              <Text style={{color:'gray', fontSize:12, flex:1, lineHeight:16, marginTop:10, marginRight:20}}>Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno subito delie variazioni del tempo.</Text>
              <View style={{flexDirection:'row', marginTop:10, justifyContent:'space-between'}}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <TouchableOpacity>
                    <Text style={{fontSize:12, color:'gray'}}>Reply</Text>
                  </TouchableOpacity>
                  <Text style={{fontSize:12, color:'gray', marginLeft:15, fontWeight:'400'}}>You Like</Text>
                </View>
                <TouchableOpacity style={{marginRight:20}}>
                  <Icon type="AntDesign" name="hearto" style={{fontSize: 20, color: 'black'}}/>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{padding:20, flexDirection:'row'}}>
            <Thumbnail source={require("../../assets/img/indineeds-search/categories-indineeds/1user.jpeg")} />
            <View style={{marginLeft:10, width:'85%'}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:14, fontWeight:'600'}}>Gwen Stefani</Text>
                <Icon type="Entypo" name="dot-single" style={{fontSize: 12, color: 'gray', width:10, marginLeft:5}}/>
                <Text style={{marginLeft:5, fontSize:12, color:'gray'}}>25 min ago</Text>
              </View>
              <Text style={{color:'gray', fontSize:12, flex:1, lineHeight:16, marginTop:10, marginRight:20}}>Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno</Text>
              <View style={{flexDirection:'row', marginTop:10, justifyContent:'space-between'}}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <TouchableOpacity>
                    <Text style={{fontSize:12, color:'gray'}}>Reply</Text>
                  </TouchableOpacity>
                  <Text style={{fontSize:12, color:'gray', marginLeft:15, fontWeight:'400'}}>You Like</Text>
                </View>
                <TouchableOpacity style={{marginRight:20}}>
                  <Icon type="AntDesign" name="hearto" style={{fontSize: 20, color: 'black'}}/>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{padding:20, flexDirection:'row'}}>
            <Thumbnail source={require("../../assets/img/indineeds-search/categories-indineeds/user2.jpeg")} />
            <View style={{marginLeft:10, width:'85%'}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:14, fontWeight:'600'}}>Britney Spears</Text>
                <Icon type="Entypo" name="dot-single" style={{fontSize: 12, color: 'gray', width:10, marginLeft:5}}/>
                <Text style={{marginLeft:5, fontSize:12, color:'gray'}}>45 min ago</Text>
              </View>
              <Text style={{color:'gray', fontSize:12, flex:1, lineHeight:16, marginTop:10, marginRight:20}}>Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno</Text>
              <View style={{flexDirection:'row', marginTop:10, justifyContent:'space-between'}}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <TouchableOpacity>
                    <Text style={{fontSize:12, color:'gray'}}>Reply</Text>
                  </TouchableOpacity>
                  <Text style={{fontSize:12, color:'gray', marginLeft:15, fontWeight:'400'}}>You Like</Text>
                </View>
                <TouchableOpacity style={{marginRight:20}}>
                  <Icon type="AntDesign" name="hearto" style={{fontSize: 20, color: 'black'}}/>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Content>
        <Card style={{width:'100%', height:65, marginBottom:0, flexDirection:'row', alignItems:'center'}}>
          <Input placeholder="Type e comment ..." style={{marginLeft:10, fontSize:14, width:'75%'}}/>
           <Button info style={{width:'25%', height:'100%', borderRadius:0, alignItems:'center', justifyContent:'center'}}>
            <Icon type="Feather" name="arrow-right" style={{fontSize: 30, color: 'white', width:30}}/>
           </Button>
        </Card>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(ProsesPembayaran);
