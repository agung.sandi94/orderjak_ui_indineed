import React, {Component} from 'react';
import { Image, View, TouchableOpacity, ScrollView, Dimensions } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Body, Right, Title, Subtitle, Button, Icon, Card, CardItem, Text, Picker, ListItem, CheckBox, Thumbnail } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';
import DatePicker from 'react-native-datepicker'

class FormBooking extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      gender : "Laki-laki",
      lama : "bulan",
      lahir : "2016-05-15",
      tnc: false
    };
  }

  onGenderChange(value: string) {
    this.setState({
      gender: value
    });
  }

  onLamaChange(value: string) {
    this.setState({
      lama: value
    });
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'white', padding:20, paddingTop:25}}>
          <Left style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <View>
              <Thumbnail small source={require('../../assets/img/example.jpg')} />
              <View style={{ position:'absolute', bottom:0, right:0, height:12, width:12, borderRadius:6, borderWidth:1, borderColor:'white', backgroundColor:'lightgray'}} />
            </View>
            <View style={{marginLeft:20}}>
              <Title style={{textAlign:'left', color:'black', width:200, fontWeight:'600'}}>Admin Support</Title>
              <Subtitle style={{textAlign:'left', color:'black', width:200, fontWeight:'600', fontSize:12}}>Terakhir online 39 menit lalu</Subtitle>
            </View>
          </Left>
          <Right>
            <TouchableOpacity>
              <Icon type="Entypo" name="dots-three-vertical" style={{fontSize:18, color:'#65c5f2', marginRight:5}} />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content style={{padding:20, paddingTop:0}}>
          <View style={{alignItems:'center', justifyContent:'center', height:Dimensions.get('window').height-270}}>
            <Icon type="Entypo" name="chat" style={{fontSize:125, color:'#65c5f2', marginRight:5}} />
            <Text style={{fontSize:18, color:'black', width:'75%', textAlign:'center', marginTop:10, fontWeight:'600'}}>Selamat datang di Chat</Text>
            <Text style={{fontSize:14, color:'gray', width:'75%', textAlign:'center', marginTop:10}}>Silahkan pilih pesan untuk memulai percakapan</Text>
          </View>
        </Content>
        <Card style={{marginBottom:0}}>
          <ScrollView horizontal={true} style={{padding:10}} showsHorizontalScrollIndicator={false}>
            <TouchableOpacity style={{borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center', padding:8, width:130, paddingLeft:15, paddingRight:15}}>
              <Text style={{color:'#65c5f2', fontSize:12, fontWeight:'600'}} numberOfLines={1}>Hai, barang ini apakah ready?</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center', padding:8, width:130, paddingLeft:15, paddingRight:15, marginLeft:10}}>
              <Text style={{color:'#65c5f2', fontSize:12, fontWeight:'600'}} numberOfLines={1}>Apakah bisa dikirim sekarang?</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center', padding:8, width:130, paddingLeft:15, paddingRight:15, marginLeft:10}}>
              <Text style={{color:'#65c5f2', fontSize:12, fontWeight:'600'}} numberOfLines={1}>Terima kasih banyak.</Text>
            </TouchableOpacity>
          </ScrollView>
        </Card>
        <Card style={{marginTop:1, marginBottom:0}}>
          <CardItem style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <TouchableOpacity>
              <Icon type="Ionicons" name="md-images" style={{fontSize:28, color:'lightgray', marginRight:5}} />
            </TouchableOpacity>
            <Input style={{padding:0, backgroundColor:'rgba(242, 247, 255,0.5)', height:40, borderColor:'lightgray', borderWidth:0.5, borderRadius:20, fontSize:12, paddingLeft:20, paddingRight:20}} placeholder="Type your message" />
            <TouchableOpacity>
              <Icon type="Ionicons" name="md-send" style={{fontSize:28, color:'#65c5f2', marginLeft:10}} />
            </TouchableOpacity>
          </CardItem>
        </Card>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(FormBooking);
