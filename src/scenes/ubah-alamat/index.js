import React, {Component} from 'react';
import { Image, View } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Button, Icon, Card, Text, Body, Right, Picker } from 'native-base';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs-new';

class UbahAlamat  extends Component {
  static navigationOptions = {
    title: "Ubah Alamat",
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      provinsi : "",
      kota : "",
      kecamatan : ""
    };
  }

  onProvinsiChange(value: string) {
    this.setState({
      provinsi: value
    });
  }

  onKotaChange(value: string) {
    this.setState({
      kota: value
    });
  }

  onKecamatanChange(value: string) {
    this.setState({
      kecamatan: value
    });
  }

  render() {
    return (
      <Container>
        <Content style={{backgroundColor:'#ffffff'}}>
          <Form>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>Nama Alamat</Label>
              <Input placeholder="Masukkan Alamat" placeholderTextColor="gray" style={{fontSize:12}}/>
            </Item>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>Nama Penerima</Label>
              <Input placeholder="Tulis Nama Penerima" placeholderTextColor="gray" style={{fontSize:12}}/>
            </Item>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>No Ponsel</Label>
              <Input placeholder="Contoh : 085221711234" placeholderTextColor="gray" style={{fontSize:12}}/>
            </Item>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>Provinsi</Label>
              <Item picker>
                <Picker
                  mode="dropdown"
                  style={{ width: '100%', color:(this.state.provinsi=="")?"gray":"black", fontSize:12, marginTop:5 }}
                  placeholderStyle={{fontSize:12}}
                  itemTextStyle={{fontSize:12}}
                  selectedValue={this.state.provinsi}
                  onValueChange={this.onProvinsiChange.bind(this)}
                >
                  <Picker.Item label="Pilih Provinsi" value="" />
                  <Picker.Item label="DKI Jakarta" value="dkiJakarta" />
                  <Picker.Item label="Jawa Barat" value="jawabarat" />
                  <Picker.Item label="Jawa Tengah" value="jawaTengah" />
                  <Picker.Item label="Jawa Timur" value="jawaTimur" />
                  <Picker.Item label="Yogyakarta" value="yogyakarta" />
                </Picker>
              </Item>
            </Item>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>Kota / Kabupaten</Label>
              <Item picker>
                <Picker
                  mode="dropdown"
                  style={{ width: '100%', color:(this.state.kota=="")?"gray":"black", fontSize:12, marginTop:5 }}
                  placeholderStyle={{fontSize:12}}
                  itemTextStyle={{fontSize:12}}
                  selectedValue={this.state.kota}
                  onValueChange={this.onKotaChange.bind(this)}
                >
                  <Picker.Item label="Pilih Kota / Kabupaten" value="" />
                  <Picker.Item label="Jakarta" value="jakarta" />
                  <Picker.Item label="Bandung" value="bandung" />
                  <Picker.Item label="Semarang" value="semarang" />
                  <Picker.Item label="Surabaya" value="surabaya" />
                  <Picker.Item label="Yogya" value="yogya" />
                </Picker>
              </Item>
            </Item>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>Kecamatan</Label>
              <Item picker>
                <Picker
                  mode="dropdown"
                  style={{ width: '100%', color:(this.state.kecamatan=="")?"gray":"black", fontSize:12, marginTop:5 }}
                  placeholderStyle={{fontSize:12}}
                  itemTextStyle={{fontSize:12}}
                  selectedValue={this.state.kecamatan}
                  onValueChange={this.onKecamatanChange.bind(this)}
                >
                  <Picker.Item label="Pilih Kecamatan" value="" />
                  <Picker.Item label="Andir" value="andir" />
                  <Picker.Item label="Sukajadi" value="sukajadi" />
                  <Picker.Item label="Setiabudi" value="setiabudi" />
                  <Picker.Item label="Pagarsih" value="pagarsih" />
                  <Picker.Item label="Suryani" value="suryani" />
                </Picker>
              </Item>
            </Item>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>Alamat Lengkap</Label>
              <Input multiline={true} numberOfLines={2} placeholder="Tulis nama perumahan, komplek, gedung, jalan, block dan nomor rumah" placeholderTextColor="gray" style={{fontSize:12}} />
            </Item>
            <Item stackedLabel style={{marginTop:15, width:'90%'}}>
              <Label style={{color:'black', fontSize:14}}>Kode Pos</Label>
              <Input placeholder="5 Digit Kode Pos" placeholderTextColor="gray" style={{fontSize:12}} />
            </Item>
            <Body style={{flexDirection:'row', width:'100%', justifyContent:'space-between', padding:20, paddingTop:40, paddingBottom:20}}>
              <Button bordered style={{width:'47%', justifyContent:'center'}}>
                <Text style={{fontSize:14, fontWeight:'600'}}>Batal</Text>
              </Button>
              <Button style={{width:'47%', justifyContent:'center'}}>
                <Text style={{fontSize:14, fontWeight:'600'}}>Simpan</Text>
              </Button>
            </Body>
          </Form>
        </Content>
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(UbahAlamat);
