/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { finishCheck } from '../login/actions'
import SplashScreen from 'react-native-smart-splash-screen'

class Example extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
  }

  componentDidMount () {
       //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
       SplashScreen.close({
          animationType: SplashScreen.animationType.scale,
          duration: 850,
          delay: 500,
       })
  }

  render() {
    return (
      <View style={styles.oval} />
    );
  }
}

const styles= StyleSheet.create({
    oval: {
        height: Dimensions.get('window').width * 2,
        width: Dimensions.get('window').width * 3,
        marginLeft: -(Dimensions.get('window').width),
        borderBottomLeftRadius: Dimensions.get('window').width*1.5,
        borderBottomRightRadius: Dimensions.get('window').width*1.5,
        backgroundColor: 'red',
        position: 'absolute',
        top: -(Dimensions.get('window').width*0.6),
    },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Example);
