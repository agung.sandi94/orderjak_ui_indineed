import React, {Component} from 'react';
import {Platform, StyleSheet, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import { login,finishCheck } from './actions'
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import Maintabs from '../../components/maintabs-new';
import CustomerService from '../../components/customer-service';
import Swiper from "react-native-web-swiper";
import Wishlist from '../../components/wishlist';
import LoadingView from '../../components/loadingView';
import { SearchBar } from 'react-native-elements';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Card, CardItem, Thumbnail } from 'native-base';


class IndineedsSearch extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state={
      activeScroll:1,
      bookmark: false
    }
  }

  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;
    return (
      <Container>
        <Content>
          <Card transparent>
            <CardItem>
              <Body style={{justifyContent:'center'}}>
                <Title style={{fontSize:22, fontWeight:'bold', color:'white'}}>Indineeds</Title>
              </Body>
              <Right style={{justifyContent:'flex-end', alignItems:'center', flexDirection:'row'}}>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate("IndineedsBasket")}}>
                  <FontAwesome name={"shopping-basket"} size={25} color={"#65c5f2"} />
                  <View style={{position:'absolute', top:-3, right:-3, backgroundColor:'red', borderColor:'white', borderWidth:2, height:16, width:16, borderRadius:8}}>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={{marginLeft:15}} onPress={()=>{this.props.navigation.navigate("Profile")}}>
                  <FontAwesome name={"user-circle-o"} size={35} color={"#65c5f2"} />
                </TouchableOpacity>
              </Right>
            </CardItem>
          </Card>
          <Item style={{backgroundColor:'rgba(242, 247, 255,0.5)', marginLeft:10, marginRight:10, paddingLeft:15, paddingRight:15, borderRadius:12}}>
            <Icon active name='search' />
            <Input placeholder='Search'/>
          </Item>
          <View style={{width:'100%',height:210, marginTop:10, justifyContent:'center', paddingLeft:10, paddingRight:10}}>
            <Swiper prevButtonText={""} nextButtonText={""} dotStyle={{backgroundColor:"lightgray"}} activeDotStyle={{backgroundColor:"white", width:25, height:8}}>
              <Card transparent style={{borderRadius:10, backgroundColor:'transparent'}}>
                <CardItem cardBody>
                  <Image style={{height:200, width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require('../../assets/img/indineeds-search/categories-indineeds/food1.jpg')}/>
                </CardItem>
              </Card>
              <Card transparent style={{borderRadius:10, backgroundColor:'transparent'}}>
                <CardItem cardBody>
                  <Image style={{height:200, width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require('../../assets/img/indineeds-search/categories-indineeds/food2.jpg')}/>
                </CardItem>
              </Card>
              <Card transparent style={{borderRadius:10, backgroundColor:'transparent'}}>
                <CardItem cardBody>
                  <Image style={{height:200, width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require('../../assets/img/indineeds-search/categories-indineeds/food3.jpg')}/>
                </CardItem>
              </Card>
            </Swiper>
          </View>
          <View style={{width:'100%',height:160, marginTop:10, justifyContent:'center', paddingLeft:10, paddingRight:10}}>
            <Swiper prevButtonText={""} nextButtonText={""} activeDotStyle={{backgroundColor:'#65c5f2'}}>
              <Card transparent style={{height:125, justifyContent:'center', alignItems:'center'}}>
                <View style={{flexDirection:'row', width:'100%', justifyContent:'space-between', alignItems:'flex-start', padding:10}}>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/1fastfood.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Fast Food</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/2vegetables.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Vegetables</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/3noodles.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Noodles</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/4seafood.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Seafood</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Card>
              <Card transparent style={{height:125, justifyContent:'center', alignItems:'center'}}>
                <View style={{flexDirection:'row', width:'100%', justifyContent:'space-between', alignItems:'flex-start', padding:10}}>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/5soup.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Soups</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/6sandwich.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Sandwiches</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/7snackfood.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Snack Foods</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ alignItems:'center', justifyContent:'center', width:'24%'}}>
                    <TouchableOpacity style={{ alignItems:'center', justifyContent:'center', alignSelf:'center' }}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/8appertizers.png')} />
                      <Text style={{marginTop:5, fontSize:14, textAlign:'center'}}>Appetizers</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Card>
            </Swiper>
          </View>
          <View style={{backgroundColor:'#eaf2ff', paddingTop:25}}>
            <Card transparent>
              <CardItem>
                <Left>
                  <Icon type="MaterialCommunityIcons" name="check-decagram" style={{color:'#65c5f2'}} />
                  <Body>
                    <Text style={{fontWeight:'bold', fontSize:20, color:'black'}}>Hot Promo</Text>
                  </Body>
                </Left>
                <Right>
                  <View style={{flexDirection:'row'}}>
                    <View style={{height:8, width:(this.state.activeScroll==1)?30:8, backgroundColor:(this.state.activeScroll==1)?'#2272f4':'#65c5f2', borderRadius:4, marginRight:5}}>
                    </View>
                    <View style={{height:8, width:(this.state.activeScroll==2)?30:8, backgroundColor:(this.state.activeScroll==2)?'#2272f4':'#65c5f2', borderRadius:4, marginRight:5}}>
                    </View>
                    <View style={{height:8, width:(this.state.activeScroll==3)?30:8, backgroundColor:(this.state.activeScroll==3)?'#2272f4':'#65c5f2', borderRadius:4, marginRight:5}}>
                    </View>
                  </View>
                </Right>
              </CardItem>
              <CardItem cardBody style={{height:240, paddingBottom:30}}>
                <ScrollView horizontal={true} style={{height:200, width:'100%', flexDirection:'row'}}
                  onScroll={(event) => {
                    var width = Dimensions.get('window').width*0.8 - 75;
                    if (event.nativeEvent.contentOffset.x < width){
                      this.setState({activeScroll:1});
                    } else if (event.nativeEvent.contentOffset.x < 2 * width){
                      this.setState({activeScroll:2});
                    } else {
                      this.setState({activeScroll:3});
                    }
                  }}
                >
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.8, padding:10, alignItems:'center'}}>
                    <Card style={{height:'100%', width:'100%', borderRadius:12}}>
                      <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/5.jpg")}/>
                    </Card>
                  </TouchableOpacity>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.8, padding:10, alignItems:'center'}}>
                    <Card style={{height:'100%', width:'100%', borderRadius:12}}>
                      <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
                    </Card>
                  </TouchableOpacity>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.8, padding:10, alignItems:'center'}}>
                    <Card style={{height:'100%', width:'100%', borderRadius:12}}>
                      <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/3.jpg")}/>
                    </Card>
                  </TouchableOpacity>
                </ScrollView>
              </CardItem>
            </Card>
            <Card transparent>
              <CardItem style={{backgroundColor:'#eaf2ff'}}>
                <Left>
                  <Icon type="Entypo" name="box" style={{color:'#65c5f2'}} />
                  <Body>
                    <Text style={{fontWeight:'bold', fontSize:20, color:'black'}}>Popular Near You</Text>
                  </Body>
                  <Right>
                    <TouchableOpacity>
                      <Text style={{color:'#65c5f2', fontSize:16, fontWeight:'500'}}>See all</Text>
                    </TouchableOpacity>
                  </Right>
                </Left>
              </CardItem>
              <CardItem cardBody style={{height:300, paddingBottom:30, paddingTop:10, backgroundColor:'#eaf2ff'}}>
                <ScrollView horizontal={true} style={{height:270, width:'100%', flexDirection:'row', paddingLeft:15, paddingRight:15}}>
                  <Card style={{borderRadius:10, marginRight:15}}>
                    <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <CardItem cardBody style={{height: '60%', width:'95%'}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/food1.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </CardItem>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray"}}/>
                        <Text style={{width:'93%', color:'gray', marginLeft:5, fontSize:14, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto Al contrario di pensi quuanto</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>RM 120</Text>
                    </TouchableOpacity>
                  </Card>
                  <Card style={{borderRadius:10, marginRight:15}}>
                    <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <CardItem cardBody style={{height: '60%', width:'95%'}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/food2.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart" style={{fontSize:22, color:"red"}}/>
                        </TouchableOpacity>
                      </CardItem>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray"}}/>
                        <Text style={{width:'93%', color:'gray', marginLeft:5, fontSize:14, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto Al contrario di pensi quuanto</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>RM 140</Text>
                    </TouchableOpacity>
                  </Card>
                  <Card style={{borderRadius:10, marginRight:15}}>
                    <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <CardItem cardBody style={{height: '60%', width:'95%'}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/food3.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </CardItem>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray"}}/>
                        <Text style={{width:'93%', color:'gray', marginLeft:5, fontSize:14, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto Al contrario di pensi quuanto</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>RM 120</Text>
                    </TouchableOpacity>
                  </Card>
                  <Card style={{borderRadius:10, marginRight:15}}>
                    <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <CardItem cardBody style={{height: '60%', width:'95%'}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/1.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </CardItem>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray"}}/>
                        <Text style={{width:'93%', color:'gray', marginLeft:5, fontSize:14, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto Al contrario di pensi quuanto</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>RM 105</Text>
                    </TouchableOpacity>
                  </Card>
                  <Card style={{borderRadius:10, marginRight:15}}>
                    <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <CardItem cardBody style={{height: '60%', width:'95%'}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/2.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </CardItem>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray"}}/>
                        <Text style={{width:'93%', color:'gray', marginLeft:5, fontSize:14, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto Al contrario di pensi quuanto</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>RM 104</Text>
                    </TouchableOpacity>
                  </Card>
                  <Card style={{borderRadius:10, marginRight:15}}>
                    <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <CardItem cardBody style={{height: '60%', width:'95%'}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/3.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </CardItem>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray"}}/>
                        <Text style={{width:'93%', color:'gray', marginLeft:5, fontSize:14, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto Al contrario di pensi quuanto</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>RM 89</Text>
                    </TouchableOpacity>
                  </Card>
                  <Card style={{borderRadius:10, marginRight:15}}>
                    <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <CardItem cardBody style={{height: '60%', width:'95%'}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10, resizeMode:'stretch'}} source={require("../../assets/img/indineeds-search/categories-indineeds/4.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </CardItem>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:15}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray"}}/>
                        <Text style={{width:'93%', color:'gray', marginLeft:5, fontSize:14, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto Al contrario di pensi quuanto</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontSize:14, fontWeight:'bold'}}>RM 190</Text>
                    </TouchableOpacity>
                  </Card>
                </ScrollView>
              </CardItem>
            </Card>
            <Card transparent>
              <CardItem>
                <Left>
                  <Icon type="Feather" name="list" style={{color:'#65c5f2', borderColor:'#65c5f2', borderWidth:2, borderRadius:5}} />
                  <Body>
                    <Text style={{fontWeight:'bold', fontSize:20, color:'black'}}>Delivering You</Text>
                  </Body>
                </Left>
              </CardItem>
              <CardItem cardBody>
                <ScrollView style={{ width:'100%'}}>
                  <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={{height:280, width:'50%', padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <Card style={{height: '60%', width:'95%', borderRadius:10}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/5.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </Card>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:5}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray", width:14, marginTop:5}}/>
                        <Text style={{ color:'gray', marginLeft:5, fontSize:12, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto ...</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontWeight:'bold', fontSize:14}}>RM 500</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{height:280, width:'50%', padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <Card style={{height: '60%', width:'95%', borderRadius:10}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/food1.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart" style={{fontSize:22, color:"red"}}/>
                        </TouchableOpacity>
                      </Card>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:5}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray", width:14, marginTop:5}}/>
                        <Text style={{ color:'gray', marginLeft:5, fontSize:12, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto ...</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontWeight:'bold', fontSize:14}}>RM 102</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={{height:280, width:'50%', padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <Card style={{height: '60%', width:'95%', borderRadius:10}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/food2.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </Card>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:5}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray", width:14, marginTop:5}}/>
                        <Text style={{ color:'gray', marginLeft:5, fontSize:12, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto ...</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontWeight:'bold', fontSize:14}}>RM 210</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{height:280, width:'50%', padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <Card style={{height: '60%', width:'95%', borderRadius:10}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/food3.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </Card>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:5}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray", width:14, marginTop:5}}/>
                        <Text style={{ color:'gray', marginLeft:5, fontSize:12, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto ...</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontWeight:'bold', fontSize:14}}>RM 80</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={{height:280, width:'50%', padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <Card style={{height: '60%', width:'95%', borderRadius:10}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/5.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </Card>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:5}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray", width:14, marginTop:5}}/>
                        <Text style={{ color:'gray', marginLeft:5, fontSize:12, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto ...</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontWeight:'bold', fontSize:14}}>RM 98</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{height:280, width:'50%', padding:5, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                      <Card style={{height: '60%', width:'95%', borderRadius:10}}>
                        <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/categories-indineeds/1.jpg")}/>
                        <TouchableOpacity style={{position:'absolute', top:5, right:5, width:22}}>
                          <Icon type="FontAwesome" name="heart-o" style={{fontSize:22, color:"gray"}}/>
                        </TouchableOpacity>
                      </Card>
                      <View style={{flexDirection:'row', width:'93%', alignItems:'center', marginTop:5}}>
                        <Icon type="FontAwesome" name="heart" style={{fontSize:14, color:"gray", width:14, marginTop:5}}/>
                        <Text style={{ color:'gray', marginLeft:5, fontSize:12, fontWeight:'500'}}>124 Likes</Text>
                      </View>
                      <Text numberOfLines={1} ellipsizeMode={"tail"} style={{width:'93%', marginTop:5, color:'black', fontSize:14}}>Al contrario di pensi quuanto ...</Text>
                      <Text style={{width:'93%', marginTop:5, color:'#65c5f2', fontWeight:'bold', fontSize:14}}>RM 105</Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
              </CardItem>
            </Card>
          </View>
        </Content>
        <CustomerService navigation={this.props.navigation} />
        <Maintabs navigate={this.props.navigation.navigate} ></Maintabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username,password) => dispatch(login(username,password)),
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(IndineedsSearch);
