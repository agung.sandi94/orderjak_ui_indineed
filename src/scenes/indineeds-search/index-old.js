import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import { login,finishCheck } from './actions'
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import Maintabs from '../../components/maintabs';
import Wishlist from '../../components/wishlist';
import LoadingView from '../../components/loadingView';
import { SearchBar } from 'react-native-elements';
import { Container, Content, Card, CardItem, Thumbnail, Button, Icon } from 'native-base';


class IndineedsSearch extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state={
      activeTab:1,
      bookmark: false
    }
  }

  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;
    return (
      <View style={styles.container}>
        <ScrollView style={{flex:1, width:'100%', marginBottom:60}}>
        <View style={{backgroundColor:'white'}}>
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:20, paddingTop: 20}}>
            <View>
              <Text style={{fontSize:22, fontWeight:'bold', color:'black'}}>Indineeds</Text>
            </View>
            <View>
              <FontAwesome name={"user-circle-o"} size={35} color={"#65c5f2"} />
            </View>
          </View>
          <View>

          <SearchBar
              containerStyle
              icon={{ name: 'search', style: { marginTop:8 } }}
              placeholder="Search Indineeds"
              placeholderTextColor={'#636363'}
              platform="android"
              containerStyle={{alignSelf: 'center', width:'90%', height:44, backgroundColor: '#e8e8e8', borderWidth: 0, borderRadius: 12 }}
              onChangeText={this.updateSearch}
              value={search}
            />

            <ScrollView horizontal={true} pagingEnabled={true} style={{height:220, width:'100%', flexDirection:'row'}}>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
              <View style={{height:'100%', width:Dimensions.get('window').width, padding:20}}>
                <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require('../../assets/img/indinews/layer-example.jpg')}/>
              </View>
            </ScrollView>
          </View>
          <View style={{marginTop: 0}}>            
              <View style={{width:'100%', height:175}}>
                <ScrollView horizontal={true} pagingEnabled={true} style={{width:'100%', height:'75%', flexDirection:'row'}}>
                  <View style={{width:Dimensions.get('window').width, height:'100%', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Elektronik.png')}/>
                      <Text style={{fontSize:12}}>Elektronik</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Perawatantubuh.png')} />
                      <Text style={{fontSize:12}}>Perawatan tubuh</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Komputer.png')} />
                      <Text style={{fontSize:12}}>Komputer</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Laptop&aksesories.png')} />
                      <Text style={{fontSize:12}}>Laptop</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:Dimensions.get('window').width, height:'100%', flexDirection:'row'}}>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Makanan&minuman.png')} />
                      <Text style={{fontSize:12}}>Makanan</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Fashionwanita.png')} />
                      <Text style={{fontSize:12}}>Fashion Wanita</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Dapur.png')} />
                      <Text style={{fontSize:12}}>Dapur</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Souvenir&kado.png')}  />
                      <Text style={{fontSize:12}}>Souvenir</Text>
                    </TouchableOpacity>  
                  </View>
                  <View style={{width:Dimensions.get('window').width, height:'100%', flexDirection:'row'}}>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Fashionpria.png')}  />
                      <Text style={{fontSize:12}}>Fashion Pria</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Buku.png')}  />
                      <Text style={{fontSize:12}}>Buku</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Buku.png')}  />
                      <Text style={{fontSize:12}}>Smartphone</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                      <Image style={{width:'60%', height:'55%', resizeMode:'contain'}} source={require('../../assets/img/indineeds-search/categories-indineeds/Buku.png')}  />
                      <Text style={{fontSize:12}}>Stationary</Text>
                    </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={{width:'100%', height:'25%', alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                  <FontAwesome name={"circle"} size={8} color={"blue"} style={{marginLeft:5}}/>
                  <FontAwesome name={"circle"} size={8} color={"skyblue"} style={{marginLeft:5}}/>
                  <FontAwesome name={"circle"} size={8} color={"skyblue"} style={{marginLeft:5}}/>
                </View>
              </View>
            </View>
            </View>

            <View style={{backgroundColor:'white', marginTop:20}}>
              <View style={{height:50, width:'100%', flexDirection:'row'}}>
                <View style={{width:'75%', height:'100%', alignItems:'center', flexDirection:'row', paddingLeft:20}}>
                  <Image style={{width:'15%', height:'40%', resizeMode:'contain'}} source={require('../../assets/img/home/crown.png')}/>
                  <Text style={{fontSize:18, fontWeight:'bold', color:'black', marginLeft:5}}>Hot Promo</Text>
                </View>
              </View>
              
              <View style={{paddingLeft:10, paddingRight:10}}> 
                <ScrollView horizontal={true} style={{height:180, width:'100%', flexDirection:'row', marginTop:10, marginBottom:10}}>
                  <TouchableOpacity style={{height:'96%', width:Dimensions.get('window').width*0.84, padding:10, alignItems:'center'}}>
                    <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/hotproduct1.png")}/>
                  </TouchableOpacity>
                  <TouchableOpacity style={{height:'96%', width:Dimensions.get('window').width*0.84, padding:10, alignItems:'center'}}>
                    <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/hotproduct2.png")}/>
                  </TouchableOpacity>
                  <TouchableOpacity style={{height:'96%', width:Dimensions.get('window').width*0.84, padding:10, alignItems:'center'}}>
                    <Image style={{height:'100%', width:'100%', borderRadius:10}} source={require("../../assets/img/indineeds-search/hotproduct1.png")}/>
                  </TouchableOpacity>
                </ScrollView>
              </View>
            </View>

            
            <View style={{marginTop:20}}>
            <View style={{height:50, width:'100%', flexDirection:'row'}}>
              <View style={{width:'75%', height:'100%', alignItems:'center', flexDirection:'row', paddingLeft:20}}>
                <Image style={{width:'15%', height:'40%', resizeMode:'contain'}} source={require('../../assets/img/home/crown.png')}/>
                <Text style={{fontSize:18, fontWeight:'bold', color:'black', marginLeft:5}}>Featured Brands</Text>
              </View>
              <View style={{width:'25%', height:'100%', justifyContent:'center', alignItems:'flex-end', paddingRight:20}}>
                <TouchableOpacity>
                  <Text style={{color:'#65c5f2'}}>See all</Text>
                </TouchableOpacity>
              </View>
            </View>
            <ScrollView horizontal={true} style={{height:280, width:'100%', flexDirection:'row', marginTop:10, marginBottom:10}}>
              <View style={{paddingLeft:18, paddingRight:18}}> 
                <Card style={{borderRadius:10}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}> 
                    <Image style={{height:'60%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                    <Text style={{width:'93%', marginTop:5, color:'#c1c1c1'}}>Like</Text>
                    <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                    <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
                </TouchableOpacity>
                </Card>
              </View>
              <View style={{paddingRight:18}}> 
                <Card style={{borderRadius:10}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <Image style={{height:'60%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                    <Text style={{width:'93%', marginTop:5, color:'#c1c1c1'}}>Like</Text>
                    <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                    <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
                </TouchableOpacity>
                </Card>
              </View>
              <View style={{paddingRight:18}}> 
                <Card style={{borderRadius:10}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <Image style={{height:'60%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                    <Text style={{width:'93%', marginTop:5, color:'#c1c1c1'}}>Like</Text>
                    <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                    <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
                </TouchableOpacity>
                </Card>
              </View>
              <View style={{paddingRight:18}}> 
                <Card style={{borderRadius:10}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <Image style={{height:'60%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                    <Text style={{width:'93%', marginTop:5, color:'#c1c1c1'}}>Like</Text>
                    <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                    <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
                </TouchableOpacity>
                </Card>
              </View>
              <View style={{paddingRight:18}}> 
                <Card style={{borderRadius:10}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <Image style={{height:'60%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                    <Text style={{width:'93%', marginTop:5, color:'#c1c1c1'}}>Like</Text>
                    <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                    <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
                </TouchableOpacity>
                </Card>
              </View>
              <View style={{paddingRight:18}}> 
                <Card style={{borderRadius:10}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <Image style={{height:'60%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                    <Text style={{width:'93%', marginTop:5, color:'#c1c1c1'}}>Like</Text>
                    <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                    <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
                </TouchableOpacity>
                </Card>
              </View>
              <View style={{paddingRight:18}}> 
                <Card style={{borderRadius:10}}>
                  <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.40, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                    <Image style={{height:'60%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                    <Text style={{width:'93%', marginTop:5, color:'#c1c1c1'}}>Like</Text>
                    <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                    <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
                </TouchableOpacity>
                </Card>
              </View>
            </ScrollView>
          </View>

          <View style={{backgroundColor:'white', marginTop:20}}>
            <View style={{height:50, width:'100%', flexDirection:'row'}}>
              <View style={{width:'75%', height:'100%', alignItems:'center', flexDirection:'row', paddingLeft:20}}>
                <Image style={{width:'15%', height:'40%', resizeMode:'contain'}} source={require('../../assets/img/home/crown.png')}/>
                <Text style={{fontSize:18, fontWeight:'bold', color:'black', marginLeft:5}}>Best selling</Text>
              </View>
            </View>
            <ScrollView horizontal={true} style={{height:236, width:'100%', flexDirection:'row', marginTop:10, marginBottom:1, paddingLeft:20}}>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-2.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
            </ScrollView>
            <ScrollView horizontal={true} style={{height:236, width:'100%', flexDirection:'row', marginTop:5, marginBottom:1, paddingLeft:20}}>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-2.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
            </ScrollView>
            <ScrollView horizontal={true} style={{height:236, width:'100%', flexDirection:'row', marginTop:5, marginBottom:10, paddingLeft:20}}>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}> 
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}} onPress={()=>this.props.navigation.navigate("NeedsDetail")}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-2.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>



        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username,password) => dispatch(login(username,password)),
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(IndineedsSearch);
